#ifndef ESCAPE_DIGRAPH_H_
#define ESCAPE_DIGRAPH_H_

#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include <algorithm>
#include <map>
#include <vector>
#include <unordered_set>

using namespace Escape;
using namespace std;

// DAG structure has two pointers, one to the 
// adjacency list of outedge, one to the adjacency list of inedges
struct CDAG
{
    CGraph outlist;
    CGraph inlist;
};

void delCDAG(CDAG DAG)
{
  delCGraph(DAG.outlist);
  delCGraph(DAG.inlist);
}

// Structure for comparing nodes according to their degree.
// So u < v if degree of u less than that of v in graph g.

struct DegreeComp
{
    CGraph *g;
    DegreeComp(CGraph *g) { this->g = g;}

    bool operator () (VertexIdx u, VertexIdx v)
    {
        VertexIdx degu = g->offsets[u+1] - g->offsets[u];  // Degree of u
        VertexIdx degv = g->offsets[v+1] - g->offsets[v];  // Degree of v
    
        if (degu < degv || (degu == degv && u < v))    // Comparing degrees and breaking ties by id
            return true;
        else
            return false;
    }
};

//Construct DAG based on degree ordering
//
// Input: Pointer for CGraph g
// Output: CDAG for degree ordering in g
//         This is the CDAG for the DAG where each edge points from lower degree endpoint to higher degree endpoint.
//
//
//         The outlist in CDAG is guaranteed to be sorted by degrees. This means that the neighbors
//         of every vertex in the outlist are sorted by their degrees in g. This is quite useful in
//         further processing.
CDAG degreeOrdered(CGraph *g)
{
    CDAG ret;     // CDAG to be returned
    CGraph outdag = {g->nVertices, 0, new EdgeIdx[g->nVertices+1], new VertexIdx[g->nEdges+1]};  // Initialize DAG of out-edges
    CGraph indag = {g->nVertices, 0, new EdgeIdx[g->nVertices+1], new VertexIdx[g->nEdges+1]};   // Initialize DAG of in-edges
    EdgeIdx outcur = 0;
    EdgeIdx incur = 0;
    VertexIdx dest;
    VertexIdx degi;
    VertexIdx degdest;

    outdag.offsets[0] = 0;
    indag.offsets[0] = 0;
    for (VertexIdx i=0; i < g->nVertices; ++i)   // Looping over all vertices in g
    {
        for (EdgeIdx j = g->offsets[i]; j < g->offsets[i+1]; ++j)   // Looping over neighbors of i in g
        {
            dest = g->nbors[j];     // We are now looking at edge (i,dest)
            degi = g->offsets[i+1] - g->offsets[i];   // Degree of i
            degdest = g->offsets[dest+1]- g->offsets[dest];   // Degree of dest
            //printf("i=%ld dest=%ld degi=%ld degdest=%ld\n",i,dest,degi,degdest);

            //We now orient the edge depending of degi vs degdest.
            // We break ties according to vertex id.
            // In the output, the g-edge (i,dest) is either pointing to dest (in if condition) or pointing to i (in else condition).

            if (degi < degdest || (degi == degdest && i < dest))   
            {
                outdag.nbors[outcur] = dest;   // We want point edge from i to dest. So this directed edge is added to outdag.
                ++outcur;                      // Increment pointer in outdag.nbors and the number of edges in outdag.
                ++outdag.nEdges;
            }
            else
            {
                indag.nbors[incur] = dest;     // We point edge from dest to i. So this edge goes into indag.
                ++incur;                       // Pointer and number of edges incremented
                ++indag.nEdges;
            }
        }
        outdag.offsets[i+1] = outcur;         // We have finished all edges incident to i, so we can update offsets in DAGs.
        indag.offsets[i+1] = incur;
    }

    for (VertexIdx i=0; i < g->nVertices;++i)  // Loops over vertices
        std::sort(outdag.nbors+outdag.offsets[i], outdag.nbors+outdag.offsets[i+1], DegreeComp(g)); // In outdag, sort all neighbors of i according to their degree. Note that DegreeComp gives the desired comparator.

    ret.outlist = outdag;
    ret.inlist = indag;

    return ret;
}

//Construct DAG based on degeneracy ordering. This only makes sense for an undirected graph
//
// Input: Pointer for CGraph g
// Output: CDAG for degeneracy ordering in g
//         This is the CDAG for the DAG where each edge points from lower order vertex endpoint to higher order endpoint according to the degree ordering.

CDAG degeneracyOrdered(CGraph *g)
{
    CDAG ret;     // CDAG to be returned
    CGraph outdag = {g->nVertices, 0, new EdgeIdx[g->nVertices+1], new VertexIdx[g->nEdges+1]};  // Initialize DAG of out-edges
    CGraph indag = {g->nVertices, 0, new EdgeIdx[g->nVertices+1], new VertexIdx[g->nEdges+1]};   // Initialize DAG of in-edges
    EdgeIdx outcur = 0;
    EdgeIdx incur = 0;
    VertexIdx dest;
    VertexIdx degi;
    VertexIdx degdest;

    outdag.offsets[0] = 0;
    indag.offsets[0] = 0;

    VertexIdx n = g->nVertices;
    VertexIdx min_deg = n;

    map <VertexIdx, unordered_set<VertexIdx> > deg_list;  
    map <VertexIdx, unordered_set<VertexIdx> > in_nbrs;  
    map <VertexIdx, unordered_set<VertexIdx> > out_nbrs;  
    map <VertexIdx, bool> touched;  
    vector<VertexIdx> cur_degs;
    cur_degs.resize(n);

    for (VertexIdx i=0; i<n; i++) // populate the map of deg -> list of vertices with that degree
    {
        VertexIdx deg = g->degree(i);
        deg_list[deg].insert(i);
        cur_degs[i] = deg;
        if (min_deg > deg) min_deg = deg;
    }

    for(VertexIdx i=0; i<n; i++)
    {
        while(deg_list[min_deg].size() == 0)
            min_deg++;
        unordered_set<VertexIdx>::iterator it = deg_list[min_deg].begin();
        VertexIdx source = *it; // current vertex
        touched[source] = true;

        deg_list[min_deg].erase(it);
        
        for (int j=g->offsets[source]; j<g->offsets[source+1]; j++)
        {
            VertexIdx nbr = g->nbors[j];
            if (touched[nbr] == true) // already processed. Add to in_nbrs of source
            {
                in_nbrs[source].insert(nbr);
                continue;
            }
            else
            {
                out_nbrs[source].insert(nbr);
                VertexIdx deg = cur_degs[nbr];
                deg_list[deg].erase(nbr);
                deg_list[deg-1].insert(nbr);
                if (deg-1 < min_deg)
                    min_deg = deg-1;
                cur_degs[nbr] = deg - 1;    
            }
        }
    }

    for(VertexIdx i=0; i<n; i++)
    {
        
        for (unordered_set<VertexIdx>::iterator in_it = in_nbrs[i].begin(); in_it!=in_nbrs[i].end(); ++in_it)
        {
            indag.nbors[incur] = *in_it;     // We point edge from dest to i. So this edge goes into indag.
            ++incur;                       // Pointer and number of edges incremented
            ++indag.nEdges;
        }
        for (unordered_set<VertexIdx>::iterator out_it = out_nbrs[i].begin(); out_it!=out_nbrs[i].end(); ++out_it)
        {
            outdag.nbors[outcur] = *out_it;     // We point edge from dest to i. So this edge goes into indag.
            ++outcur;                       // Pointer and number of edges incremented
            ++outdag.nEdges;
        }
        outdag.offsets[i+1] = outcur;         // We have finished all edges incident to i, so we can update offsets in DAGs.
        indag.offsets[i+1] = incur;
    }
    
    ret.outlist = outdag;
    ret.inlist = indag;

    return ret;
}

#endif



