#ifndef ESCAPE_NEAR_CLIQUES_BRUTE_FORCE_H_
#define ESCAPE_NEAR_CLIQUES_BRUTE_FORCE_H_

#include <algorithm>
#include <chrono>
#include <random>
#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/Utils.h"
#include "Escape/nCr.h"
#include "Escape/CliqueHelper.h"
// #include "Escape/NearCliquesTuranShadow_missing_edges.h"
#include "JointSort.h"

using namespace Escape;
using namespace std;
using namespace std::chrono;

/* Between this and NearCliquesBruteForceDirected, this is the optimized one because for k=7
it looks for a 5 clique instead of a 6 clique and maintains the common neighbors as it goes along,
thus saving time.
*/

void near_cliques_brute_force(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, unsigned long long clique_size)
{    
    high_resolution_clock::time_point t_elim_s = high_resolution_clock::now();
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;
    
    Stack* stack = newStack();
    VertexSet *path = NULL, *nbrs = NULL, *X = NULL;
    double num_cliques = 0, num_near_cliques = 0, num_internal_nodes = 0;
    int h = clique_size - 2;

    for (VertexIdx c=0; c<CG.nVertices; c++)
    {
        path = newVertexSet(1);
        path->vertices[0] = c;
        nbrs = newVertexSet(DG.outlist.offsets[c+1] - DG.outlist.offsets[c]);
        std::copy(DG.outlist.nbors+DG.outlist.offsets[c], DG.outlist.nbors+DG.outlist.offsets[c+1], nbrs->vertices);
        X = newVertexSet(CG.degree(c));
        std::copy(CG.nbors+CG.offsets[c], CG.nbors+CG.offsets[c+1], X->vertices);
        
        PartialClique* pc = newPartialClique(path, nbrs, X);
        stack->push(pc);

        StackItem* si = stack->pop();

        while (si != NULL)
        {
            path = si->pc->path;
            nbrs = si->pc->nbrs;  // get the current path and its nbrs
            X = si->pc->X;
            VertexIdx psize = path->nVertices, nsize = nbrs->nVertices;
            int flag = 0;
            if (psize + nsize < h)
            {
            }
            else if (psize == h)
            {   
                num_internal_nodes++;
            
                VertexSet *allnbrs = si->pc->X; //get_common_nbrs(CG, path);

                for (int i=0; i<allnbrs->nVertices; i++)
                {
                    for (int j=i+1; j<allnbrs->nVertices; j++)
                    {
                        if (CG.isEdge(allnbrs->vertices[i], allnbrs->vertices[j]) == -1)
                            num_near_cliques++;
                    }
                }
            }
            else
            {
                for (VertexIdx i=0; i<nsize; i++)
                {
                    VertexIdx v = nbrs->vertices[i];
                    VertexSet *new_path = newVertexSet(psize+1);
                    std::copy(path->vertices, path->vertices+path->nVertices, new_path->vertices);
                    new_path->vertices[psize] = v;

                    VertexSet *new_X = newVertexSet(X->nVertices);
                    int k = 0;
                    for (int j=0; j<X->nVertices; j++)
                    {
                        if (CG.isEdge(X->vertices[j],v) != -1)
                        {
                            new_X->vertices[k] = X->vertices[j];
                            k++;
                        }
                    }
                    new_X->nVertices = k;

                    VertexSet *new_nbrs = newVertexSet(nsize-1-i);
                    k = 0;
                    for (int j=i+1; j<nsize; j++)
                    {
                        if (CG.isEdge(v, nbrs->vertices[j]) != -1)
                        {
                            new_nbrs->vertices[k] = nbrs->vertices[j];
                            k++;
                        }
                    }
                    new_nbrs->nVertices = k; 
                    if ((((psize+1)<h) && (k == 0)) || (new_X->nVertices == 0))
                    {
                        delete[] new_nbrs->vertices;
                        delete new_nbrs;
                        delete[] new_path->vertices;
                        delete new_path;
                        delete[] new_X->vertices;
                        delete new_X;
                        continue;
                    }
                    else
                    {
                        PartialClique *pc = newPartialClique(new_path, new_nbrs, new_X);
                        stack->push(pc);
                    }
                }
            }
            
        	if (nbrs->nVertices >= 0) delete[] nbrs->vertices;
            delete nbrs;
            if (path->nVertices >= 0) delete[] path->vertices;
            delete path;
            if (X->nVertices >= 0) delete[] X->vertices;
            delete X;
            delete si->pc;
            delete si;
            si = stack->pop();
        }
    }

    delStack(stack);

    high_resolution_clock::time_point t_elim_e = high_resolution_clock::now();
    auto duration_program = std::chrono::duration_cast<std::chrono::microseconds>( t_elim_e - t_elim_s ).count();
    
    cout << "time reqd: " << duration_program << endl;
    cout << "number of near cliques = " << num_near_cliques << endl;
    cout << "number of num_internal_nodes = " << num_internal_nodes << endl;
	cout << "near-clique size = " << clique_size << endl;
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;

	of << clique_size << "," << num_near_cliques << "," << duration_program << "," << currentDateTime() << endl;
}

#endif
