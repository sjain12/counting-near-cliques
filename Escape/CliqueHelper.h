#ifndef ESCAPE_CLIQUE_HELPER_H_
#define ESCAPE_CLIQUE_HELPER_H_

#include <algorithm>
#include <chrono>
#include <unordered_set>
#include <time.h>

#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/Utils.h"
#include "Escape/nCr.h"
#include "JointSort.h"

using namespace Escape;
using namespace std;
using namespace std::chrono;

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}
///////////////////////////////////////////////////

struct VertexSet
{
    VertexIdx  nVertices;       //number of vertices in the VertexSet
    VertexIdx *vertices;        //array of vertices in the VertexSet

    bool contains(VertexIdx v) const;
    void print(FILE* f = stdout) const;
};

VertexSet* newVertexSet(VertexIdx nVertices)
{
    VertexSet *vs = new VertexSet();
    vs->nVertices = nVertices;
    vs->vertices = new VertexIdx[nVertices];
    return vs;
}

VertexSet* newVertexSet(VertexIdx * vertices, VertexIdx nVertices)
{
    VertexSet *vs = new VertexSet();
    vs->nVertices = nVertices;
    vs->vertices = vertices;
    return vs;
}

bool VertexSet::contains(VertexIdx v) const
{
    for (int i=0; i<nVertices; i++)
        if (vertices[i] == v)
            return true;
    return false;
}

void VertexSet::print(FILE* f) const
{
    if (nVertices == 0) fprintf(f, "Set empty\n");

    for (VertexIdx i = 0; i < nVertices; ++i)
        fprintf(f, "%lld   ", vertices[i]);
}

void delVertexSet(VertexSet* v)
{
    if (v != NULL)
    {
        if (v->nVertices != 0)
            delete[] v->vertices;
        delete v;
    }
}

VertexSet* intersect(VertexSet* v1, VertexSet* v2) 
{
    VertexSet *s=v2, *l=v1;
    if (v1->nVertices < v2->nVertices)
    {
        s = v1;
        l = v2;
    }

    VertexSet* v = newVertexSet(s->nVertices);
    int j = 0;
    for (int i=0; i<s->nVertices;i++)
    {
        if (l->contains(s->vertices[i]))
        {
            v->vertices[j] = s->vertices[i];
            j++;
        }
    }
    v->nVertices = j;
    return v;
}

struct PartialClique
{
    VertexSet* path;
    VertexSet* nbrs;
    VertexSet* X;
};

PartialClique* newPartialClique(VertexSet* path, VertexSet* nbrs, VertexSet* X=NULL)
{
    PartialClique* pc = new PartialClique;
    pc->path = path;
    pc->nbrs = nbrs;
    pc->X = X;
    return pc;
}

void delPartialClique(PartialClique* pc)
{
    if (pc != NULL)
    {
        delVertexSet(pc->path);
        delVertexSet(pc->nbrs);
        delVertexSet(pc->X);
        delete pc;
    }
}

struct StackItem
{
    PartialClique *pc;
    StackItem *next;
};

void delStackItem(StackItem* si)
{
    if (si != NULL)
    {
        delPartialClique(si->pc);
        delete si;
    }
}


struct Stack
{
    StackItem *top;

    void push(PartialClique* pc);
    StackItem* pop();
    int i;
};

Stack* newStack()
{
    Stack* s = new Stack();
    s->top = NULL;
    s->i = 0;
    return s;
}

void delStack(Stack* s)
{
    if (s != NULL)
    {
        s->top = NULL;
        delete s;
    }
}

void Stack::push(PartialClique* pc)
{
    StackItem* si = new StackItem;
    si->pc = pc;
    si->next = top;
    top = si;
    i++;
}

StackItem* Stack::pop()
{
    StackItem *si = top;
    if (top == NULL) return NULL;
    top = si->next;
    si->next = NULL;
    i--;
    return si;
}

StackItem* newStackItem(PartialClique *pc, StackItem *next)
{
    StackItem* si = new StackItem;
    si->pc = pc;
    si->next = next;
    return si;
}

bool isClique(VertexIdx *vertices, int n, CGraph &CG)
{
    for (int i=0; i<n-1; i++)
    {
        for (int j=i+1; j<n; j++)
            if (CG.isEdge(vertices[i], vertices[j]) == -1)
                return false;
    }
    return true;
}

////////////////////////

// returns the k-core of nbrs. keeps nbrs intact, produces a new VertexSet to return
VertexSet * get_degree_core(CGraph &CG, VertexSet *nbrs, int k)
{
    VertexIdx n = nbrs->nVertices;
    
    vector<VertexIdx> deg_list(n,0);

    
    // for (int i=0; i<n; i++)
    // {
    //     VertexIdx v = nbrs->vertices[i];
    //     VertexIdx degv = 0;
    //     for (int j=i+1; j<n; j++)
    //     {
    //         VertexIdx u = nbrs->vertices[j];
    //         if (CG.isEdge(v,u) != -1)
    //         {
    //             deg_list[i]++;
    //             deg_list[j]++;
    //         }
    //     }
    // }

    VertexSet *core = newVertexSet(nbrs->nVertices);
    std::copy(nbrs->vertices, nbrs->vertices+nbrs->nVertices, core->vertices);

    // int count = 0;
    // for (int j=0; j<n; j++)
    // {
    //     if (deg_list[j]<k) continue;
    //     VertexIdx v = nbrs->vertices[j];
    //     core->vertices[count] = v;
    //     count++;
    //     // iterate over deg_list and populate core, set size of core and return
    // }
    // core->nVertices = count;
    return core;
}

// // returns the k-core of nbrs. keeps nbrs intact, produces a new VertexSet to return
// VertexSet * get_core(CGraph &CG, VertexSet *nbrs, int k)
// {
//     VertexIdx n = nbrs->nVertices;
//     VertexIdx min_deg = n;
//     double degen = 0;

//     map <VertexIdx, unordered_set<VertexIdx> > deg_list;    
//     map <VertexIdx, bool> touched;
//     map <VertexIdx, VertexIdx> cur_degs;
//     // cur_degs.resize(n);
//     //deg_list.resize(40000);
//     //printf("before for\n");
//     for (int i=0; i<n; i++)
//     {
//         VertexIdx v = nbrs->vertices[i];
//         VertexIdx deg = 0;
//         for (int j=0; j<n; j++)
//         {
//             VertexIdx u = nbrs->vertices[j];
//             if (CG.isEdge(v,u) != -1) deg++;
//         }
//         deg_list[deg].insert(v);
//         cur_degs[v] = deg;
//         if (min_deg > deg) min_deg = deg;
//     }

//     VertexSet *core = newVertexSet(nbrs->nVertices);
//     if (min_deg >= k) 
//     { 
//         std::copy(nbrs->vertices, nbrs->vertices+nbrs->nVertices, core->vertices);
//         return core;
//     }
//     //printf("after for\n");
//     for(int i=0; i<n; i++)
//     {
//     //printf("Inside first for\n");
//         while(deg_list[min_deg].size() == 0)
//             min_deg++;

//         if (min_deg >= k)
//         {
//             // unordered_set<VertexIdx>::iterator it = deg_list[min_deg].begin();
//             int count = 0;
//             for (int j=0; j<n; j++)
//             {
//                 VertexIdx v = nbrs->vertices[j];
//                 if (touched[v] == true) continue;
//                 // cout << "Found valid k-core member" << endl;
//                 core->vertices[count] = v;
//                 count++;
//                 // iterate over deg_list and populate core, set size of core and return
//             }
//             core->nVertices = count;
//             return core;
//         }
//         // else
//         unordered_set<VertexIdx>::iterator it = deg_list[min_deg].begin();
//         VertexIdx source = *it;
//         touched[source] = true;

//         deg_list[min_deg].erase(it);
//        //printf("Calling second for\n"); 
//         for (int j=0; j<n; j++)
//         {
//             VertexIdx nbr = nbrs->vertices[j];
//             if ((touched[nbr] == true) || (CG.isEdge(source,nbr) == -1)) continue;
//         // for (int j=g.offsets[source]; j<g.offsets[source+1]; j++)
//         // {
//         //   VertexIdx nbr = g.nbors[j];
//         //   if (touched[nbr] == true)
//         //       continue;
//             VertexIdx deg = cur_degs[nbr];
//             deg_list[deg].erase(nbr);
//             deg_list[deg-1].insert(nbr);
//             if (deg-1 < min_deg)
//                 min_deg = deg-1;
//             cur_degs[nbr] = deg - 1;    
//         }
//     }
//     core->nVertices = 0;
//     // printf("returning degen = %f\n", degen);
//     return core;
// }

/* calculates the density of the nbrs. also gets degree of every vertex, 
does degree ordering of the vertices, and gets child, grandchild pair 
*/
double get_density(CGraph &CG, VertexSet *nbrs, EdgeIdx& num_edges)
{
    double n = nbrs->nVertices;
    double total = 0;
    double actual = 0;
    if (n==0) {num_edges = 0; return 0;}
    if (n==1) {num_edges = 0; return 1;}

    for (int i=0; i<n-1; i++)
    {
        for (int j=i+1; j<n; j++)
        {
            total++;
            if (CG.isEdge(nbrs->vertices[i], nbrs->vertices[j]) != -1)
            {
                actual++;
            }
        }
    }

    double density = actual/total;

    num_edges = actual;
    return density;
}

/* calculates the density of the nbrs. also gets degree of every vertex, 
does degree ordering of the vertices, and gets child, grandchild pair 
*/
double get_density_and_reorder(CGraph &CG, VertexSet *nbrs, EdgeIdx& num_edges)
{
    double n = nbrs->nVertices;
    double total = 0;
    double actual = 0;
    if (n==0) return 0;
    if (n==1) return 1;

    int *degrees = (int *) calloc(n, sizeof(int));

    for (int i=0; i<n-1; i++)
    {
        for (int j=i+1; j<n; j++)
        {
            total++;
            if (CG.isEdge(nbrs->vertices[i], nbrs->vertices[j]) != -1)
            {
                degrees[i]++;
                degrees[j]++;
                actual++;
            }
        }
    }

    double density = actual/total;

    VertexIdx tmp;

    for (int i=n-2; i>=0; i--)
    {
        for (int j=0; j<=i; j++)
        {
            if (degrees[j] > degrees[j+1])
            {
                tmp = degrees[j];
                degrees[j] = degrees[j+1];
                degrees[j+1] = tmp;
                tmp = nbrs->vertices[j];
                nbrs->vertices[j] = nbrs->vertices[j+1];
                nbrs->vertices[j+1] = tmp;
            }
        }
    }
    num_edges = actual;
    free(degrees);
    return density;
}

/* calculates the density of the nbrs. also gets degree of every vertex, 
does degree ordering of the vertices, and gets child, grandchild pair 
*/
double get_density_degrees_and_reorder(CGraph &CG, VertexSet *nbrs, EdgeIdx& num_edges, int *degrees)
{
    double n = nbrs->nVertices;
    double total = 0;
    double actual = 0;
    if (n==0) return 0;
    if (n==1) return 1;

    // degrees = (int *) calloc(n, sizeof(int));

    for (int i=0; i<n-1; i++)
    {
        for (int j=i+1; j<n; j++)
        {
            total++;
            if (CG.isEdge(nbrs->vertices[i], nbrs->vertices[j]) != -1)
            {
                degrees[i]++;
                degrees[j]++;
                actual++;
            }
        }
    }

    double density = actual/total;

    VertexIdx tmp;

    for (int i=n-2; i>=0; i--)
    {
        for (int j=0; j<=i; j++)
        {
            if (degrees[j] > degrees[j+1])
            {
                tmp = degrees[j];
                degrees[j] = degrees[j+1];
                degrees[j+1] = tmp;
                tmp = nbrs->vertices[j];
                nbrs->vertices[j] = nbrs->vertices[j+1];
                nbrs->vertices[j+1] = tmp;
            }
        }
    }
    num_edges = actual;

    // cout << "In degree ordering:" << endl;
    // for (int i=0; i<n-1; i++)
    // {
    //     cout << "v = " << nbrs->vertices[i] << " degree = " << degrees[i] << endl;
    // }
    // free(degrees);
    return density;
}


// double get_density_vector(CGraph CG, vector<VertexIdx> &nbrs, EdgeIdx& num_edges)
// {
//     double n = nbrs.size();
//     double total = 0;
//     double actual = 0;
//     if (n==0) return 0;
//     if (n==1) return 1;

//     // int *degrees = (int *) calloc(n, sizeof(int));

//     for (int i=0; i<n-1; i++)
//     {
//         for (int j=i+1; j<n; j++)
//         {
//             total++;
//             if (CG.isEdge(nbrs[i], nbrs[j]) != -1)
//             {
//                 // degrees[i]++;
//                 // degrees[j]++;
//                 actual++;
//             }
//         }
//     }

//     double density = actual/total;

//  //    VertexIdx tmp;

//  //    for (int i=n-2; i>=0; i--)
//  //    {
//  //        for (int j=0; j<=i; j++)
//  //        {
//  //            if (degrees[j] > degrees[j+1])
//  //            {
//  //                tmp = degrees[j];
//  //                degrees[j] = degrees[j+1];
//  //                degrees[j+1] = tmp;
//  //                tmp = nbrs->vertices[j];
//  //                nbrs->vertices[j] = nbrs->vertices[j+1];
//  //                nbrs->vertices[j+1] = tmp;
//  //            }
//  //        }
//  //    }
//     num_edges = actual;
//     // free(degrees);
//     return density;
// }

VertexSet * get_common_nbrs(CGraph &CG, VertexSet *sample)
{
    int l = CG.nVertices;
    for (int i=0; i<sample->nVertices; i++)
    {
        VertexIdx v = sample->vertices[i];
        if (l>CG.degree(v)) l = CG.degree(v);
    }

    VertexSet * nbrs = newVertexSet(l);
    int k=0;
    VertexIdx v = sample->vertices[0];
    bool flag = true;
    for (int i=0; i<CG.degree(v); i++)
    {
        VertexIdx nbrv = CG.nbors[CG.offsets[v] + i];
        flag = true;
        for (int j=1; j<sample->nVertices; j++)
        {
            if (CG.isEdge(nbrv, sample->vertices[j]) == -1)
            {
                flag = false;
                break;
            }
        }
        if (flag == true)
        {
            nbrs->vertices[k] = nbrv;
            k++;
        }
    }
    nbrs->nVertices = k;
    return nbrs;
}

VertexSet * get_common_nbrs_cc(CGraph &CG, VertexSet *sample, vector<VertexIdx> &color, vector<VertexIdx> &missing_colors)
{
    int l = CG.nVertices;
    for (int i=0; i<sample->nVertices; i++)
    {
        VertexIdx v = sample->vertices[i];
        if (l>CG.degree(v)) l = CG.degree(v);
    }

    VertexSet * nbrs = newVertexSet(l);
    int k=0;
    VertexIdx v = sample->vertices[0];
    bool flag = true;
    for (int i=0; i<CG.degree(v); i++)
    {
        VertexIdx nbrv = CG.nbors[CG.offsets[v] + i];
        if (find(missing_colors.begin(), missing_colors.end(), color[nbrv]) == missing_colors.end()) continue; // if color of nbr is not one of the missing colors then don't consider it.
        flag = true;
        for (int j=1; j<sample->nVertices; j++)
        {
            if (CG.isEdge(nbrv, sample->vertices[j]) == -1)
            {
                flag = false;
                break;
            }
        }
        if (flag == true)
        {
            nbrs->vertices[k] = nbrv;
            k++;
        }
    }
    nbrs->nVertices = k;
    return nbrs;
}

double get_num_22_cliques(CGraph &CG, VertexSet *sample)
{
    // for u,w in clique, u<w, find all nbrsu (x) such that they are connected to all in clique except w and w < x
    // find all nbrsw (v) of w that are connected to all in clique but u
    // if (x,w) connected, near_clique++
    // cout << "Here" << endl;
    int clique_size = sample->nVertices;
    map<VertexIdx, VertexSet*> all_but;
    VertexIdx max_deg = 0;
    double num_near_cliques = 0;
    for (int i=0; i<clique_size; i++)
    {
        if (max_deg < CG.degree(sample->vertices[i])) max_deg = CG.degree(sample->vertices[i]);
    }

    for (int i=0; i<clique_size; i++)
    {
        VertexIdx key = sample->vertices[i];
        all_but[key] = newVertexSet(max_deg);
        all_but[key]->nVertices = 0;
    }

    // cout << "Here1" << endl;
    VertexIdx v = sample->vertices[0];
    for (int i=0; i<CG.degree(v); i++)
    {
        VertexIdx nbrv = CG.nbors[CG.offsets[v] + i];
        VertexIdx missing_vertex = CG.nVertices;
        VertexIdx degs = 0;
        int flag_clique_vertex = 0;
        // check if v is connected to all others except one, if the id of that one is < nbrv
        for (int j=1; j<clique_size; j++)
        {
            if (nbrv == sample->vertices[j]) {flag_clique_vertex = 1; break;}
            if (CG.isEdge(nbrv, sample->vertices[j]) == -1) missing_vertex = sample->vertices[j];
            else degs++;
        }
        if (flag_clique_vertex == 1) continue;
        if (degs == clique_size-2) 
        {
            all_but[missing_vertex]->vertices[all_but[missing_vertex]->nVertices] = nbrv;
            all_but[missing_vertex]->nVertices++;
        }
    }

    // cout << "Here2" << endl;

    VertexIdx u = sample->vertices[1];
    for (int i=0; i<CG.degree(u); i++)
    {
        VertexIdx nbru = CG.nbors[CG.offsets[u] + i];
        if (CG.isEdge(v, nbru) != -1) continue;
        // if (v >= nbru) continue;
        int flag = 0;
        for (int j=2; j<clique_size; j++)
        {
            if (CG.isEdge(nbru, sample->vertices[j]) == -1) {flag = 1; break;}
        }
        if (flag == 1) continue;
        all_but[v]->vertices[all_but[v]->nVertices] = nbru;
        all_but[v]->nVertices++;
    }

    // cout << "Here3" << endl;
    // int l = CG.nVertices;
    // for (int i=0; i<sample->nVertices; i++)
    // {
    //     VertexIdx v = sample->vertices[i];
    //     if (l>CG.degree(v)) l = CG.degree(v);
    // }

    for (int i=0; i<clique_size; i++)
    {
        for (int j=i+1; j<clique_size; j++)
        {
            // if (i == j) continue;

            VertexIdx u = sample->vertices[i];
            VertexIdx w = sample->vertices[j];
            if (u > w) 
            {
                VertexIdx temp = u;
                u = w;
                w = temp;
            }
            // loop over all_but of w to get candidates for x
                // loop over all_but of u to get candidates for v
                    // if w < x, u < v and (v,x) exists, increment num_near_cliques
            for (int k=0; k<all_but[w]->nVertices; k++)
            {
                VertexIdx x = all_but[w]->vertices[k];
                for (int l=0; l<all_but[u]->nVertices; l++)
                {
                    VertexIdx v = all_but[u]->vertices[l];
                    if ((w < x) && (u < v) && (CG.isEdge(v,x) != -1)) num_near_cliques++; 
                }
            }
            
        }
    }

    for (int i=0; i<clique_size; i++)
    {
        VertexIdx w = sample->vertices[i];
        delete[] all_but[w]->vertices;
        delete all_but[w];
    }

    return num_near_cliques;
}

double get_num_22_cliques_cc(CGraph &CG, VertexSet *sample, vector<VertexIdx> &color, vector<VertexIdx> &missing_colors)
{
    // for u,w in clique, u<w, find all nbrsu (x) such that they are connected to all in clique except w and w < x
    // find all nbrsw (v) of w that are connected to all in clique but u
    // if (x,w) connected, near_clique++
    int clique_size = sample->nVertices;
    map<VertexIdx, VertexSet*> all_but;
    VertexIdx max_deg = 0;
    double num_near_cliques = 0;
    for (int i=0; i<clique_size; i++)
    {
        if (max_deg < CG.degree(sample->vertices[i])) max_deg = CG.degree(sample->vertices[i]);
    }

    for (int i=0; i<clique_size; i++)
    {
        VertexIdx key = sample->vertices[i];
        all_but[key] = newVertexSet(max_deg);
        all_but[key]->nVertices = 0;
    }

    VertexIdx v = sample->vertices[0];
    for (int i=0; i<CG.degree(v); i++)
    {
        VertexIdx nbrv = CG.nbors[CG.offsets[v] + i];
        VertexIdx missing_vertex = CG.nVertices;
        VertexIdx degs = 0;
        int flag_clique_vertex = 0;
        // check if v is connected to all others except one, if the id of that one is < nbrv
        for (int j=1; j<clique_size; j++)
        {
            if (nbrv == sample->vertices[j]) {flag_clique_vertex = 1; break;}
            if (CG.isEdge(nbrv, sample->vertices[j]) == -1) missing_vertex = sample->vertices[j];
            else degs++;
        }
        if (flag_clique_vertex == 1) continue;
        if ((degs == clique_size-2) && (find(missing_colors.begin(), missing_colors.end(), color[nbrv]) != missing_colors.end()))
        {
            all_but[missing_vertex]->vertices[all_but[missing_vertex]->nVertices] = nbrv;
            all_but[missing_vertex]->nVertices++;
        }
    }

    VertexIdx u = sample->vertices[1];
    for (int i=0; i<CG.degree(u); i++)
    {
        VertexIdx nbru = CG.nbors[CG.offsets[u] + i];
        if (CG.isEdge(v, nbru) != -1) continue;
        // if (v >= nbru) continue;
        int flag = 0;
        for (int j=2; j<clique_size; j++)
        {
            if (CG.isEdge(nbru, sample->vertices[j]) == -1) {flag = 1; break;}
        }
        if (flag == 1) continue;
        if (find(missing_colors.begin(), missing_colors.end(), color[nbru]) != missing_colors.end())
        {
            all_but[v]->vertices[all_but[v]->nVertices] = nbru;
            all_but[v]->nVertices++;
        }
    }

    // int l = CG.nVertices;
    // for (int i=0; i<sample->nVertices; i++)
    // {
    //     VertexIdx v = sample->vertices[i];
    //     if (l>CG.degree(v)) l = CG.degree(v);
    // }

    for (int i=0; i<clique_size; i++)
    {
        for (int j=i+1; j<clique_size; j++)
        {
            // if (i == j) continue;

            VertexIdx u = sample->vertices[i];
            VertexIdx w = sample->vertices[j];
            if (u > w) 
            {
                VertexIdx temp = u;
                u = w;
                w = temp;
            }
            // loop over all_but of w to get candidates for x
                // loop over all_but of u to get candidates for v
                    // if w < x, u < v and (v,x) exists, increment num_near_cliques
            for (int k=0; k<all_but[w]->nVertices; k++)
            {
                VertexIdx x = all_but[w]->vertices[k];
                for (int l=0; l<all_but[u]->nVertices; l++)
                {
                    VertexIdx v = all_but[u]->vertices[l];
                    if ((w < x) && (u < v) && (CG.isEdge(v,x) != -1) && (color[v] != color[x])) num_near_cliques++; 
                }
            }
            
        }
    }

    for (int i=0; i<clique_size; i++)
    {
        VertexIdx w = sample->vertices[i];
        delete[] all_but[w]->vertices;
        delete all_but[w];
    }
    
    return num_near_cliques;
}


VertexIdx get_num_partial_nbrs(CGraph &CG, VertexSet *sample, int drop)
{
    int k = sample->nVertices;
    double num_partial_nbrs = 0;
    VertexIdx max_deg = 0;
    // unordered_set<VertexIdx> nbrs;
    for (int i=0; i<k; i++)
    {
        if (CG.degree(sample->vertices[i]) > max_deg) max_deg = CG.degree(sample->vertices[i]);
    }
    vector<VertexIdx> nbrs;
    nbrs.reserve(2*max_deg);
    //vector<VertexIdx> nbrs(CG.nVertices);

    for (int i=0; i<drop+1; i++)
    {
        VertexIdx v = sample->vertices[i];
        for (int j=0; j<CG.degree(v); j++)
        {
            VertexIdx nbrv = CG.nbors[CG.offsets[v] + j];
            // if (nbrs.find(nbrv) == nbrs.end())
            // if (nbrs[nbrv] == 0)
            if (find(nbrs.begin(), nbrs.end(), nbrv) == nbrs.end())
            {
                // nbrs.insert(nbrv);
                nbrs.push_back(nbrv);
                int num_nbrv_nbrs = 0;
                for (int l=0; l<k; l++)
                {
                    if ((l!=i) && (CG.isEdge(nbrv, sample->vertices[l]) != -1))
                        num_nbrv_nbrs++;
                }
                if (num_nbrv_nbrs == (k-drop-1)) num_partial_nbrs++;
                // cout << "num_nbrv_nbrs = " << num_nbrv_nbrs << endl;
            }
        }
    }
    return num_partial_nbrs;
}

VertexIdx get_num_partial_nbrs_cc(CGraph &CG, VertexSet *sample, int drop, vector<VertexIdx> &color)
{
    int k = sample->nVertices;
    VertexIdx missing_color = -2;
    vector<int> col_pallette (k+1,0);
    for (int i=0; i<k; i++)
    {
        col_pallette[color[sample->vertices[i]]] = 1;
    }
    for (int i=0; i<k+1; i++)
    {
        if (col_pallette[i] == 0) missing_color = i;
    }

    double num_partial_nbrs = 0;
    VertexIdx max_deg = 0;
    // unordered_set<VertexIdx> nbrs;
    for (int i=0; i<k; i++)
    {
        if (CG.degree(sample->vertices[i]) > max_deg) max_deg = CG.degree(sample->vertices[i]);
    }
    vector<VertexIdx> nbrs;
    nbrs.reserve(2*max_deg);
    //vector<VertexIdx> nbrs(CG.nVertices);

    for (int i=0; i<drop+1; i++)
    {
        VertexIdx v = sample->vertices[i];
        for (int j=0; j<CG.degree(v); j++)
        {
            VertexIdx nbrv = CG.nbors[CG.offsets[v] + j];
            // if (nbrs.find(nbrv) == nbrs.end())
            // if (nbrs[nbrv] == 0)
            if ((color[nbrv] == missing_color) && (find(nbrs.begin(), nbrs.end(), nbrv) == nbrs.end()))
            {
                // nbrs.insert(nbrv);
                nbrs.push_back(nbrv);
                int num_nbrv_nbrs = 0;
                for (int l=0; l<k; l++)
                {
                    if ((l!=i) && (CG.isEdge(nbrv, sample->vertices[l]) != -1))
                        num_nbrv_nbrs++;
                }
                if (num_nbrv_nbrs == (k-drop-1)) num_partial_nbrs++;
                // cout << "num_nbrv_nbrs = " << num_nbrv_nbrs << endl;
            }
        }
    }
    return num_partial_nbrs;
}


VertexSet* get_partial_nbrs(CGraph &CG, VertexSet *sample, int drop)
{
    int k = sample->nVertices;
    double num_partial_nbrs = 0;
    // unordered_set<VertexIdx> nbrs;
    VertexIdx max_deg = 0;
    // unordered_set<VertexIdx> nbrs;
    for (int i=0; i<k; i++)
    {
        if (CG.degree(sample->vertices[i]) > max_deg) max_deg = CG.degree(sample->vertices[i]);
    }
    vector<VertexIdx> nbrs;
    nbrs.reserve(2*max_deg);
    int c=0;
    // unordered_set<VertexIdx> valid_nbrs;
    // vector<VertexIdx> valid_nbrs(CG.nVertices, 0);
    VertexSet *partial_nbrs = newVertexSet(2*max_deg);
    for (int i=0; i<drop+1; i++)
    {
        VertexIdx v = sample->vertices[i];
        for (int j=0; j<CG.degree(v); j++)
        {
            VertexIdx nbrv = CG.nbors[CG.offsets[v] + j];
            // if (nbrs.find(nbrv) == nbrs.end())
            if (find(nbrs.begin(), nbrs.end(), nbrv) == nbrs.end())
            {
                // nbrs.insert(nbrv);
                nbrs.push_back(nbrv);
                // nbrs.insert(nbrv);
                // nbrs[nbrv] = 1;
                int num_nbrv_nbrs = 0;
                for (int l=0; l<k; l++)
                {
                    if ((l!=i) && (CG.isEdge(nbrv, sample->vertices[l]) != -1))
                        num_nbrv_nbrs++;
                }
                if (num_nbrv_nbrs == (k-drop-1))
                {
                    // valid_nbrs.insert(nbrv);
                    partial_nbrs->vertices[c] = nbrv;
                    c++;
                }

                // if (num_nbrv_nbrs == (k-drop-1)) num_partial_nbrs++;
                // cout << "num_nbrv_nbrs = " << num_nbrv_nbrs << endl;
            }
        }
    }
    // VertexSet *partial_nbrs = newVertexSet(valid_nbrs.size());
    // for (auto it = valid_nbrs.begin(); it != valid_nbrs.end(); ++it)
    // {
    //     partial_nbrs->vertices[c] = *it;
    //     c++;
    // }

    partial_nbrs->nVertices = c;
    return partial_nbrs;
}

VertexSet * get_common_nbrs(CGraph &CG, VertexIdx v, VertexIdx u)
{
    VertexIdx l=u, h=v;
    int ln = CG.degree(l);
    int hn = CG.degree(h);
    if (hn < ln)
    {
        int temp = ln;
        ln = hn;
        hn = temp;
        l = v;
        h = u;
    }

    // vector<VertexIdx> ret;
    // ret.reserve(ln);
    VertexSet *ret = newVertexSet(ln);
    int k=0;

    for (int i=0; i<ln; i++)
    {
        for (int j=0; j<hn; j++)
        {
            if (CG.nbors[CG.offsets[l] + i] == CG.nbors[CG.offsets[h] + j])
            {
                ret->vertices[k] = CG.nbors[CG.offsets[l] + i];
                k++;
            }
                // ret.push_back(CG.nbors[CG.offsets[l] + i]);
        }
    }
    ret->nVertices = k;

    return ret;
}



// vector<VertexIdx> get_common_vertices(CGraph &CG, vector<VertexIdx> vu, VertexIdx w)
// {
//     int ln = vu.size();
//     int hn = CG.degree(w);
//     if (hn < ln)
//     {
//         int temp = ln;
//         ln = hn;
//         hn = temp;
//     }

//     vector<VertexIdx> ret;
//     ret.reserve(ln);

//     for (int i=0; i<vu.size(); i++)
//     {
//         for (int j=0; j<CG.degree(w); j++)
//         {
//             if (vu[i] == CG.nbors[CG.offsets[w] + j])
//                 ret.push_back(vu[i]);
//         }
//     }

//     return ret;
// }

/* checks if anything in X survives after applying sample_vec */
// bool is_empty_X(CGraph &CG, vector<VertexIdx> &X, vector<VertexIdx> &sample_vec)
// {
//     bool found_missing_edge=false, found_vertex = false;
//     for (int i=0; i<X.size(); i++)
//     {
//         found_missing_edge = false;
//         for (int j=0; j<sample_vec.size(); j++)
//         {
//             if (CG.isEdge(X[i], sample_vec[j]) == -1)
//             {
//                 found_missing_edge = true;
//                 break;
//             }
//         }
//         if (found_missing_edge == false)
//         {
//             return false;
//         }
//     }
//     return true;
// }

/* checks if anything in X survives after applying sample_vec */
bool is_empty_X(CGraph &CG, VertexSet *X, VertexSet *sample_vec)
{
    bool found_missing_edge=false, found_vertex = false;
    for (int i=0; i<X->nVertices; i++)
    {
        found_missing_edge = false;
        for (int j=0; j<sample_vec->nVertices; j++)
        {
            if (CG.isEdge(X->vertices[i], sample_vec->vertices[j]) == -1)
            {
                found_missing_edge = true;
                break;
            }
        }
        if (found_missing_edge == false)
        {
            return false;
        }
    }
    return true;
}

#endif
