#ifndef ESCAPE_NEAR_22_CLIQUES_COLOR_CODING_H_
#define ESCAPE_NEAR_22_CLIQUES_COLOR_CODING_H_

#include <algorithm>
#include <chrono>
#include <random>
#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/Utils.h"
#include "Escape/nCr.h"
#include "Escape/CliqueHelper.h"
#include "JointSort.h"

using namespace Escape;
using namespace std;
using namespace std::chrono;

void near_22_cliques_color_coding(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, int nc_size)
{    
    high_resolution_clock::time_point t_elim_s = high_resolution_clock::now();
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;
    Stack* stack = newStack();
    VertexSet *path = NULL, *nbrs = NULL, *X = NULL; // X will store the colors not seen so far
    vector<VertexIdx> color;
    color.resize(CG.nVertices);
    double num_cliques = 0, est = 0;
    std::random_device rd{}; // use to seed the rng 
    std::mt19937 rng{rd()}; // rng

    int clique_size = nc_size - 2;
    double num_near_cliques = 0;

    std::uniform_int_distribution<int> distribution(0,nc_size-1);
    int col = 0;
    for (VertexIdx i=0; i<CG.nVertices; i++)
    {
        col = distribution(rng); 
        color[i] = col;
    }

    // VertexSet *orig_X = newVertexSet(nc_size);
    // for (int i=0; i<nc_size; i++)
    //     orig_X->vertices[i] = i;

    for (VertexIdx c=0; c<CG.nVertices; c++)
    {
        path = newVertexSet(1);
        path->vertices[0] = c;
        nbrs = newVertexSet(DG.outlist.offsets[c+1] - DG.outlist.offsets[c]);
        std::copy(DG.outlist.nbors+DG.outlist.offsets[c], DG.outlist.nbors+DG.outlist.offsets[c+1], nbrs->vertices);
        // X = newVertexSet(nc_size);
        // std::copy(orig_X->vertices, orig_X->vertices+orig_X->nVertices, X->vertices);
        // X 
        PartialClique* pc = newPartialClique(path, nbrs);
        stack->push(pc);

        StackItem* si = stack->pop();
        while (si != NULL)
        {
            path = si->pc->path;
            nbrs = si->pc->nbrs;  // get the current path and its nbrs
            VertexIdx psize = path->nVertices, nsize = nbrs->nVertices;

            if (psize + nsize < clique_size)
            {
            }
            else if (psize == clique_size)
            {   
                // // num_cliques++;
                // VertexSet *allnbrs = get_partial_nbrs(CG, path, 2);
                // // cout << "Found a clique" << endl;

                vector<VertexIdx> missing_colors;
                vector<int> col_pallette (nc_size,0);
                for (int i=0; i<psize; i++)
                {
                    col_pallette[color[path->vertices[i]]] = 1;
                }
                for (int i=0; i<nc_size; i++)
                {
                    if (col_pallette[i] == 0) missing_colors.push_back(i);
                }

                num_near_cliques += get_num_22_cliques_cc(CG, path, color, missing_colors);
                // for (int i=0; i<allnbrs->nVertices; i++)
                // {
                //     int flag = 1;
                //     for (int l=0; l<psize; l++)
                //     {
                //         if (color[allnbrs->vertices[i]] == color[path->vertices[l]]) 
                //         {
                //             flag = 0; 
                //             break;
                //         }
                        
                //     }
                //     if (flag == 1)
                //     {
                //         num_near_cliques++;
                //     }
                // }
                // if (allnbrs->nVertices >= 0) delete[] allnbrs->vertices;
                // delete allnbrs;
            }
            else
            {
                for (VertexIdx i=0; i<nsize; i++)
                {
                    VertexIdx v = nbrs->vertices[i];
                    int flag = 0;
                    for (int j=0; j<psize; j++)
                    {
                        if (color[path->vertices[j]] == color[v])
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1) continue;

                    VertexSet *new_path = newVertexSet(psize+1);
                    std::copy(path->vertices, path->vertices + psize, new_path->vertices);
                    new_path->vertices[path->nVertices] = v;   //added v to path

                    VertexSet *new_nbrs = newVertexSet(nsize-1-i);
                    int k = 0;
                    for (int j=i+1; j<nsize; j++)
                    {
                        if ((CG.isEdge(v, nbrs->vertices[j]) != -1) && (color[v] != color[nbrs->vertices[j]]))
                        {
                            new_nbrs->vertices[k] = nbrs->vertices[j];
                            k++;
                        }
                    }

                    // if (k == 0) 
                    // {
                    //     delete[] new_nbrs->vertices;
                    //     delete new_nbrs;
                    //     delete[] new_path->vertices;
                    //     continue;
                    // }
                    // else
                    // {
                        new_nbrs->nVertices = k; 
                        PartialClique *pc = newPartialClique(new_path, new_nbrs);
                        stack->push(pc);
                    // }

                    // int s = DG.outlist.offsets[v+1]-DG.outlist.offsets[v], l = nsize;
                    // if (l < s)
                    // {
                    //     l = s;
                    //     s = nsize;
                    // }

                    // VertexSet *common = newVertexSet(s);
                    // int k = 0, offset = DG.outlist.offsets[v];
                    // for (int l =0; l < nbrs->nVertices; l++) {
                    //     for (int j=0; j<DG.outlist.offsets[v+1]-DG.outlist.offsets[v]; j++) {
                    //         if (nbrs->vertices[l] == DG.outlist.nbors[offset + j])
                    //         {
                    //             common->vertices[k] = nbrs->vertices[l];
                    //             k++;
                    //             break;
                    //         }
                    //     }
                    // }

                    // common->nVertices = k; 
                    // PartialClique *pc = newPartialClique(new_path, common);
                    // stack->push(pc);
                }
            }
            
        	if (nbrs->nVertices >= 0) delete[] nbrs->vertices;
            delete nbrs;
            if (path->nVertices >= 0) delete[] path->vertices;
            delete path;
            delete si->pc;
            delete si;
            si = stack->pop();
        }
    }

    delStack(stack);

    high_resolution_clock::time_point t_elim_e = high_resolution_clock::now();
    auto duration_program = std::chrono::duration_cast<std::chrono::microseconds>( t_elim_e - t_elim_s ).count();
    
    double kfac = 1;

    // for (int i=1; i<=clique_size; i++)
    //     kfac *= i;
    // est = num_near_cliques * pow(clique_size, clique_size) / kfac; 
    for (int i=1; i<=nc_size; i++)
        kfac *= i;
    est = num_near_cliques * pow(nc_size, nc_size) / kfac; 

    cout << "time reqd: " << duration_program << endl;
	cout << "num_near_cliques = " << num_near_cliques << endl;
	cout << "near-clique size = " << nc_size << endl;
    cout << "estimated number of near_cliques = " << est << endl;
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;

	of << nc_size << "," << est << "," << duration_program << "," << currentDateTime() << endl;
}

#endif
