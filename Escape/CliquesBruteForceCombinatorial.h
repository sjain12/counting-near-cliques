#ifndef ESCAPE_CLIQUES_BRUTE_FORCE_COMBINATORIAL_H_
#define ESCAPE_CLIQUES_BRUTE_FORCE_COMBINATORIAL_H_

#include <algorithm>
#include <chrono>
#include <random>
#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/Utils.h"
#include "Escape/nCr.h"
#include "Escape/CliqueHelper.h"
#include "JointSort.h"

using namespace Escape;
using namespace std;
using namespace std::chrono;

/* Code for brute force counting of cliques. Does only 1 thing different from CliquesBruteForce. 
Gets density of nbrs and if density is 1, just adds nCr to num_cliques instead of expanding 
the nbrs */

double cliques_brute_force_combinatorial(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, unsigned long long clique_size)
{    
    high_resolution_clock::time_point t_elim_s = high_resolution_clock::now();
    
    Stack* stack = newStack();
    VertexSet *path = NULL, *nbrs = NULL;
    double num_cliques = 0;

    for (VertexIdx c=0; c<CG.nVertices; c++)
    {
        path = newVertexSet(1);
        path->vertices[0] = c;
        nbrs = newVertexSet(DG.outlist.offsets[c+1] - DG.outlist.offsets[c]);
        std::copy(DG.outlist.nbors+DG.outlist.offsets[c], DG.outlist.nbors+DG.outlist.offsets[c+1], nbrs->vertices);
        PartialClique* pc = newPartialClique(path, nbrs);
        stack->push(pc);

        StackItem* si = stack->pop();

        while (si != NULL)
        {
            path = si->pc->path;
            nbrs = si->pc->nbrs;  // get the current path and its nbrs
            VertexIdx psize = path->nVertices, nsize = nbrs->nVertices;

            int flag = 0;
            if (psize + nsize < clique_size)
            {
            }
            else if (psize == clique_size)
            {   
                num_cliques++;
            }
            else if (psize == clique_size - 1)
            {
                num_cliques += nsize;
            }
            else if (psize == clique_size - 2)
            {
                for (int i=0; i<nsize-1; i++)
                    for (int j=i+1; j<nsize; j++)
                        if (CG.isEdge(nbrs->vertices[i], nbrs->vertices[j]) != -1)
                            num_cliques++;
            }
            else
            {
                EdgeIdx num_edges = 0;
                double density = get_density(CG, nbrs, num_edges);
                if (density == 1.0)
                {
                    num_cliques += nCr[nsize][clique_size-psize];
                }
                else
                {
                    /* Convert nbrs into a DAG. This is supposed to happen by doing a degree ordering of vertices in nbrs.
                    Instead, we simply use the index ordering in nbrs. While this may not give the best ordering, 
                    it saves the time of having to sort the vertices in nbrs by degree.

                    We have to spawn a branch for every v in nbrs, and whichever vertices from nbrs following v (in
                    the index ordering of nbrs) is a neighbor of v, should be added to common (and eventually new_path)
                    */
                    for (VertexIdx i=0; i<nsize-1; i++)
                    {
                        VertexIdx v = nbrs->vertices[i]; //v is the new vertex to be added to path

                        VertexSet *new_path = newVertexSet(psize+1); /* Note: we are not actually updating new_path, just its length
                        since we only need the length */

                        VertexSet *new_nbrs = newVertexSet(nsize-1-i);
                        int k = 0;
                        for (int j=i+1; j<nsize; j++)
                        {
                            if (CG.isEdge(v, nbrs->vertices[j]) != -1)
                            {
                                new_nbrs->vertices[k] = nbrs->vertices[j];
                                k++;
                            }
                        }


                        // for (int l =0; l < nbrs->nVertices; l++) {
                        //     for (int j=0; j<DG.outlist.offsets[v+1]-DG.outlist.offsets[v]; j++) {
                        //         if (nbrs->vertices[l] == DG.outlist.nbors[offset + j])
                        //         {
                        //             common->vertices[k] = nbrs->vertices[l];
                        //             k++;
                        //             break;
                        //         }
                        //     }
                        // } Note: Changed by Shweta in this version of the code (need to change in original TuranShadow!)
                        if (k == 0) 
                        {
                            delete[] new_nbrs->vertices;
                            delete new_nbrs;
                            delete[] new_path->vertices;
                            continue;
                        }
                        else
                        {
                            new_nbrs->nVertices = k; 
                            PartialClique *pc = newPartialClique(new_path, new_nbrs);
                            stack->push(pc);
                        }
                    }
                }
            }
            
            if (nbrs->nVertices > 0) delete[] nbrs->vertices;
            delete nbrs;
            if (path->nVertices > 0) delete[] path->vertices;
            delete path;
            delete si->pc;
            delete si;
            si = stack->pop();
        }
    }

    delStack(stack);

    high_resolution_clock::time_point t_elim_e = high_resolution_clock::now();
    auto duration_program = std::chrono::duration_cast<std::chrono::microseconds>( t_elim_e - t_elim_s ).count();
    
    cout << "time reqd: " << duration_program << endl;
    cout << "number of cliques = " << num_cliques << endl;
    cout << "clique size = " << clique_size << endl;

    of << clique_size << "," << num_cliques << "," << duration_program << "," << endl;
    return num_cliques;
}

#endif
