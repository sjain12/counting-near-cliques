#ifndef ESCAPE_NEAR_2_CLIQUES_FEW_LEAVES_SAMPLING_H_
#define ESCAPE_NEAR_2_CLIQUES_FEW_LEAVES_SAMPLING_H_

#include "Escape/CliqueHelper.h"
#include <cassert>
#include <vector>
#include <map>
#include <random>
#include <ctgmath>
#include <iomanip>
#include <climits>

using namespace Escape;
using namespace std;

bool DEBUG = false;

void near_2_cliques_turan_shadow_few_leaves_sampling(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, int runs, int num_samples, int nc_size)
{    
    double num_cliques = 0, num_leaves = 0;
    double non_leaf_cliques = 0;
    
    int k = 0; 
    double succ_ratio = 0, leaf_samples = 0, leaf_size = 0, density = 0, leaf_cliques = 0;
    VertexIdx psize = 0, nsize = 0;
    PartialClique *pc = NULL;
    VertexIdx arr[10000];  // array for storing numbers 0 through leaf_size, which will be used by the random number generator
    VertexIdx rng[10000]; // will be used for the random number generator

    double avg_est = 0;

    int num_subtrees = 0;
    
    
    int clique_size = nc_size - 1;


    for (int i=0; i<10000; i++)
        arr[i] = i;

    double internal_nodes = 0;
   
    srand(time(NULL));
    
    Stack *stack = newStack();
    

    double sigma = 0, sum_of_wts = 0, cliques_counted = 0;
    vector<double> degrees(CG.nVertices,0);
    vector<VertexSet *> cored_outnbrs(CG.nVertices);
    high_resolution_clock::time_point prep_s = high_resolution_clock::now();
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;

    for (VertexIdx i=0; i<CG.nVertices; i++)
    {
        // if (DG.outlist.degree(i) < (clique_size-1)) continue;
          // get the clique_size-2-core
        VertexSet* outnbrs = newVertexSet(DG.outlist.degree(i));

        std::copy(DG.outlist.nbors+DG.outlist.offsets[i], DG.outlist.nbors+DG.outlist.offsets[i+1], outnbrs->vertices);

        // VertexSet* new_outnbrs = get_degree_core(CG, outnbrs, clique_size-2);
        // if (outnbrs->nVertices >= 0) delete[] outnbrs->vertices;
        // delete outnbrs;

        // if (new_outnbrs->nVertices < (clique_size-1)) 
        // {
        //     if (new_outnbrs->nVertices >= 0) delete[] new_outnbrs->vertices;
        //     delete new_outnbrs;
        //     continue;
        // }
   
        // EdgeIdx n_edges = 0;
        // double out_density = get_density(CG, new_outnbrs, n_edges);
        // if (n_edges < ((clique_size - 1)*(clique_size - 2)/2)) continue;

        degrees[i] = nCr[outnbrs->nVertices][clique_size-1];
        sigma += degrees[i];
        cored_outnbrs[i] = outnbrs;
        
        // cout << "Node " << i << " neighbors of i ";
        // for (VertexIdx j=0; j<new_outnbrs->nVertices; j++)
        //     cout << " " << new_outnbrs->vertices[j];
        // cout << endl;
        // VertexSet* outnbrs = newVertexSet(DG.outlist.degree(i));
        // EdgeIdx n_edges = 0;
        // std::copy(DG.outlist.nbors+DG.outlist.offsets[i], DG.outlist.nbors+DG.outlist.offsets[i+1], outnbrs->vertices);
        // double out_density = get_density(CG, outnbrs, n_edges);
        // if (n_edges < ((clique_size - 1)*(clique_size - 2)/2)) continue;

        // degrees[i] = nCr[DG.outlist.offsets[i+1] - DG.outlist.offsets[i]][clique_size-1];
        // original_sigma += degrees[i];
    }

    high_resolution_clock::time_point prep_e = high_resolution_clock::now();
    auto duration_prep = std::chrono::duration_cast<std::chrono::microseconds>( prep_e - prep_s ).count();

    // std::default_random_engine generator_nodes;
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::discrete_distribution<unsigned long long> distribution_nodes (degrees.begin(), degrees.end());

    string datfile(gname + "_" + to_string(nc_size) + "_" + to_string(num_samples) + "_" + to_string(runs) + "_nc2_data_fl");
    std::string dfname = "../results/cliques/" + datfile;
    std::cout << dfname << std::endl;
    std::ofstream df;
    df.open(dfname);
    if (!df.is_open())
    {
        std::cout << "Could not open output file." << std::endl;
        exit(1);
    }
    printf("Output File: %s\n", dfname.c_str());
    // df << "estimate,succ_ratio,time,prep" << endl;
    df << "est,succ_ratio,sum_of_weights,t_treegen,t_prep,num_subtrees,empty_samples,failed_samples," << endl;

    // df.precision(2);
    double vertex_cliques = 0;

    string outfile(gname + "_" + to_string(nc_size) + "_" + to_string(num_samples) + "_nc2_params_fl");
    std::string ofname = "../results/cliques/" + outfile;
    std::cout << ofname << std::endl;
    std::ofstream ofparams;
    ofparams.open(ofname);
    if (!ofparams.is_open())
    {
        std::cout << "Could not open output file." << std::endl;
        exit(1);
    }
    printf("Output File: %s\n", ofname.c_str());

    ofparams << "internal," << "leaves," << "discarded leaves,"  << "shadow size," << "N,";
    ofparams << "tree_t," << "num_samples," << endl;

    cout << "est,succ_ratio,sum_of_weights,t_treegen,t_prep,num_subtrees,empty_samples,failed_samples," << endl;
    for (int run =0; run < runs; run++)
    {
        cout << "\n \n";
        high_resolution_clock::time_point treegen_s = high_resolution_clock::now();
        sum_of_wts = 0;
        cliques_counted = 0;
        num_subtrees = 0;
        double shadow_size = 0;
        double discarded_leaves = 0;
        int empty_samples = 0;
        int failed_samples = 0;
        map<VertexIdx, double> m_num_samples;
        double samples_processed = 0;
        double internal = 0;
        for (int sample = 0; sample < num_samples; sample++) 
        {
            VertexIdx w = distribution_nodes(gen);
            if (m_num_samples.find(w) != m_num_samples.end())
            {
                m_num_samples[w]++;
            }
            else
            {
                m_num_samples[w] = 1;
                num_subtrees++;     
            }
        }
        for (map<VertexIdx, double>::iterator it = m_num_samples.begin(); it != m_num_samples.end(); ++it)
        {
            double num_near_cliques = 0;
            vector<int> leafk;
            vector<int> leafn;
            vector <VertexSet*> leaves;
            vector <VertexSet*> paths;
            vector <double> leafExperiments;
            double N = 0;
            int leaves_inc = 100000;
            leafExperiments.reserve(leaves_inc);
            leaves.reserve(leaves_inc);
            paths.reserve(leaves_inc);
            leafk.reserve(leaves_inc);
            leafn.reserve(leaves_inc);
            leaf_cliques = 0;
            num_leaves = 0;
            double max_leaves = leaves_inc;
    

            VertexIdx w = it->first;
            samples_processed += it->second;
            
            VertexSet *path = newVertexSet(1);
            path->vertices[0] = w;
            VertexSet *nbrs = newVertexSet(DG.outlist.offsets[w+1] - DG.outlist.offsets[w]);
            std::copy(DG.outlist.nbors+DG.outlist.offsets[w], DG.outlist.nbors+DG.outlist.offsets[w+1], nbrs->vertices);
            pc = newPartialClique(path, nbrs);
            stack->push(pc);

            StackItem* si = stack->pop();
            int flag = 0;
            EdgeIdx num_edges = 0;
            vertex_cliques = 0;

            while (si != NULL)  // unraveling the stack
            {
                VertexSet *path = si->pc->path;
                VertexSet *nbrs = si->pc->nbrs;  // get the current path and its nbrs
                psize = path->nVertices;
                nsize = nbrs->nVertices;
                flag = 0;

                k = clique_size - psize;
                density = get_density_and_reorder(CG, nbrs,num_edges);
                        
                if (psize + nsize < clique_size) 
                {
                    discarded_leaves++;
                    flag = 1;
                }
                else
                {
                    double thresh_dens = 1.0 - pow((k-1), -1);
                    if (density >= thresh_dens)
                    {
                        leaves.push_back(nbrs);
                        paths.push_back(path);
                        // cout << "psize = " << psize << endl;
                        // for (int ww = 0; ww < psize; ww++)
                        //     // cout << path->vertices[ww] << " ";
                        //     cout << paths[num_leaves]->vertices[ww] << " ";
                        // cout << "\nafter psize\n" << endl;

                        leafExperiments.push_back(nCr[nsize][k]);
                        leafk.push_back(k);
                        leafn.push_back(nsize);
                        N += nCr[nsize][k];
                        shadow_size += nsize;

                        num_leaves++;
                        if (num_leaves == max_leaves) 
                        {
                            max_leaves += leaves_inc;
                            leaves.reserve(max_leaves);
                            paths.reserve(max_leaves);
                            leafExperiments.reserve(max_leaves);
                            leafk.reserve(max_leaves);
                            leafn.reserve(max_leaves);
                        }
                    }
                    else
                    {
                        internal++;
                        for (int i=0; i<nbrs->nVertices; i++)
                        {
                            VertexIdx v = nbrs->vertices[i];
                            VertexSet *new_path = newVertexSet(psize+1);
                            std::copy(path->vertices, path->vertices+path->nVertices, new_path->vertices);
                            new_path->vertices[psize] = v;

                            VertexSet *new_nbrs = newVertexSet(nsize-1-i);

                            int c = 0;
                            for (int j=i+1; j<nbrs->nVertices; j++)
                            {
                                if (CG.isEdge(v, nbrs->vertices[j]) != -1)
                                {
                                    new_nbrs->vertices[c] = nbrs->vertices[j];
                                    c++;
                                }
                            }

                            if (((psize+1)<clique_size) && (c == 0))
                            {
                                delete[] new_nbrs->vertices;
                                delete new_nbrs;
                                delete[] new_path->vertices;
                                continue;
                            }
                            else
                            {
                                new_nbrs->nVertices = c; 
                                PartialClique *pc = newPartialClique(new_path, new_nbrs);
                                stack->push(pc);
                            }
                            // new_nbrs->nVertices = c;
                    
                            // PartialClique *pc1 = newPartialClique(new_path, new_nbrs);
                            // stack->push(pc1);
                        }

                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    if (nbrs->nVertices >= 0) delete[] nbrs->vertices;
                    delete nbrs;
                    
                // }
                    if (path->nVertices >= 0) delete[] path->vertices;
                    delete path;
                }
                delete si->pc;
                delete si;
                si = stack->pop();
            }
            if (num_leaves == 0) 
            {
              empty_samples += it->second; // CHANGE
              // sigma -= degrees[it->first];
            }
            // cout << "num_leaves = " << num_leaves << endl;
            if (num_leaves != 0)
            {
                std::default_random_engine generator;
                std::discrete_distribution<unsigned long long> distribution (leafExperiments.begin(), leafExperiments.end());

                double leaf_cliques = 0;
                for (int b=0; b<it->second; b++) 
                {
                    int leaf = distribution(generator);
                    k = leafk[leaf];
                    VertexSet *l = leaves[leaf];
                    VertexSet *sample_path = paths[leaf];
                    // cout << "leaf = " << leaf << endl;
                    // for (int ww = 0; ww < clique_size-k; ww++)
                    //     cout << l->vertices[ww] << " ";
                    std::copy(arr, arr+l->nVertices, rng);
                    std::random_shuffle ( rng, rng+l->nVertices);
                    // VertexIdx *sample = (VertexIdx *) calloc(k,sizeof(VertexIdx)); 
                    VertexIdx *sample = (VertexIdx *) calloc(k,sizeof(VertexIdx)); 
                    for (int j = 0; j < k; j++)
                        sample[j] = l->vertices[rng[j]];

                    // cout << " before isClique" << endl;
                    if (isClique(sample, k, CG)) 
                    {
                        leaf_cliques++; //experiment was a success. Sampled set of vertices do form a clique
                        VertexSet *clique = newVertexSet(clique_size);
                        // cout << " k = " << k << endl;
                        // cout << " sample_path->nVertices = " << sample_path->nVertices << endl;
                        // for (int ww = 0; ww < clique_size-k; ww++)
                        //     cout << sample_path->vertices[ww] << " ";
                        // cout << endl;
                        std::copy(sample_path->vertices, sample_path->vertices+(clique_size-k), clique->vertices);
                        // cout << " inside isClique" << endl;
                        std::copy(sample, sample+k, clique->vertices+(clique_size-k));

                        num_near_cliques += get_num_partial_nbrs(CG, clique, 2);

                        if (clique->nVertices >= 0) delete[] clique->vertices;
                        delete clique;
                        // cout << "num_near_cliques = " << num_near_cliques << endl;
                        

                        // VertexSet *allnbrs = get_common_nbrs(CG, clique);

                        // for (int i=0; i<allnbrs->nVertices; i++)
                        // {
                        //     for (int j=i+1; j<allnbrs->nVertices; j++)
                        //     {
                        //         if (CG.isEdge(allnbrs->vertices[i], allnbrs->vertices[j]) == -1)
                        //             num_near_cliques++;
                        //     }
                        // }
                        // cout << " leaving isClique" << endl;
                    }
                    else
                        failed_samples++;
                    // cout << " after isClique" << endl;
                    free(sample);
                }
                // sum_of_wts += (leaf_cliques*N/degrees[i]);
                sum_of_wts += (num_near_cliques*N/degrees[w]);
            }
            for (long int li = 0; li < leaves.size(); li++)
            {
                if (leaves[li]->nVertices >= 0) delete[] leaves[li]->vertices;
                delete leaves[li];
                if (paths[li]->nVertices >= 0) delete[] paths[li]->vertices;
                delete paths[li];
            }

        }
        high_resolution_clock::time_point treegen_e = high_resolution_clock::now();
        auto duration_treegen = std::chrono::duration_cast<std::chrono::microseconds>( treegen_e - treegen_s ).count();

        double est = sum_of_wts * sigma / (double) num_samples;

        df << est << "," << sum_of_wts << "," << sum_of_wts / (double) (num_samples) << "," << duration_treegen << "," << duration_prep << "," << num_subtrees << "," << empty_samples << "," << failed_samples << "," << endl;

        cout << est << "," << sum_of_wts << "," <<  sum_of_wts / (double) (num_samples) << "," << duration_treegen << "," << duration_prep << "," << num_subtrees << "," << empty_samples << "," << failed_samples << "," << endl;

        ofparams << internal << "," << num_leaves << "," << discarded_leaves << "," << shadow_size << "," << sigma << ",";
        ofparams << duration_treegen + duration_prep << "," << num_subtrees << "," << endl;
        ofparams << endl;


        // df << est << "," <<  sum_of_wts / (double) num_samples << "," << duration_treegen << "," << duration_prep << "," << num_subtrees << endl;
        // cout << est << "," <<  sum_of_wts / (double) num_samples << "," << duration_treegen << "," << duration_prep << "," << num_subtrees << endl;
    
        // ofparams << internal << "," << num_leaves << "," << discarded_leaves << "," << shadow_size << "," << sigma << ",";
        // ofparams << duration_treegen << "," << num_subtrees << "," << endl;
        // ofparams << endl;

        avg_est += est;
        
    }
    df.close();
    ofparams.close();
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;
    cout << "near_clique_size = " << nc_size << endl;
    cout << "internal = " << internal << endl;
    cout << "Number of near-cliques counted = " << cliques_counted << endl;
    cout << "sigma = " << sigma << endl;
    cout << "Average est over all runs = " << avg_est / runs << endl;
}


// void cliques_turan_shadow_few_leaves_sampling(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, int runs, int num_samples, int clique_size)
// {    
//     double num_cliques = 0, num_leaves = 0;
//     double non_leaf_cliques = 0;
//     double discarded_leaves = 0;
//     int k = 0; 
//     double succ_ratio = 0, leaf_samples = 0, leaf_size = 0, density = 0, leaf_cliques = 0;
//     VertexIdx psize = 0, nsize = 0;
//     PartialClique *pc = NULL;
//     VertexIdx arr[10000];  // array for storing numbers 0 through leaf_size, which will be used by the random number generator
//     VertexIdx rng[10000]; // will be used for the random number generator

    
//     double internal = 0;


//     for (int i=0; i<10000; i++)
//         arr[i] = i;

//     double internal_nodes = 0;
// 	double shadow_size = 0;
//     srand(time(NULL));
    
//     Stack *stack = newStack();
    

//     double sigma = 0, sum_of_wts = 0, cliques_counted = 0;
//     vector<double> degrees(CG.nVertices,0);

//     for (VertexIdx i=0; i<CG.nVertices; i++)
//     {
//         degrees[i] = nCr[DG.outlist.offsets[i+1] - DG.outlist.offsets[i]][clique_size-1];
//         sigma += degrees[i];
//     }

//     // std::default_random_engine generator_nodes;
//     std::random_device rd;  //Will be used to obtain a seed for the random number engine
//     std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//     std::discrete_distribution<double> distribution_nodes (degrees.begin(), degrees.end());

//     string datfile(gname + "_" + to_string(clique_size) + "_" + to_string(num_samples) + "_" + to_string(runs) + "_data_fl");
//     std::string dfname = "../results/cliques/" + datfile;
//     std::cout << dfname << std::endl;
//     std::ofstream df;
//     df.open(dfname);
//     if (!df.is_open())
//     {
//         std::cout << "Could not open output file." << std::endl;
//         exit(1);
//     }
//     printf("Output File: %s\n", dfname.c_str());
//     df << "estimate,succ_ratio,time," << endl;
//     df.precision(2);
//     double vertex_cliques = 0;

//     string outfile(gname + "_" + to_string(clique_size) + "_" + to_string(num_samples) + "_params_fl");
//     std::string ofname = "../results/cliques/" + outfile;
//     std::cout << ofname << std::endl;
//     std::ofstream ofparams;
//     ofparams.open(ofname);
//     if (!ofparams.is_open())
//     {
//         std::cout << "Could not open output file." << std::endl;
//         exit(1);
//     }
//     printf("Output File: %s\n", ofname.c_str());

//     ofparams << "internal," << "leaves," << "discarded leaves,"  << "shadow size," << "N,";
//     ofparams << "tree_t," << "num_samples," << endl;

//     for (int run =0; run < runs; run++)
//     {
//         cout << "\n \n";
//         high_resolution_clock::time_point treegen_s = high_resolution_clock::now();
//         sum_of_wts = 0;
//         cliques_counted = 0;
//         map<VertexIdx, double> m_num_samples;
//         double samples_processed = 0;
//         for (int sample = 0; sample < num_samples; sample++) 
//         {
//             VertexIdx i = distribution_nodes(gen);
//             if (m_num_samples.find(i) != m_num_samples.end())
//             {
//                 m_num_samples[i]++;
//             }
//             else
//                 m_num_samples[i] = 1;
//         }
//         for (map<VertexIdx, double>::iterator it = m_num_samples.begin(); it != m_num_samples.end(); ++it)
//         {
//             vector<int> leafk;
//             vector<int> leafn;
//             vector <VertexSet*> leaves;
//             vector <double> leafExperiments;
//             double N = 0;
//             int leaves_inc = 100000;
//             leafExperiments.reserve(leaves_inc);
//             leaves.reserve(leaves_inc);
//             leafk.reserve(leaves_inc);
//             leafn.reserve(leaves_inc);
//             leaf_cliques = 0;
//             num_leaves = 0;
    

//             VertexIdx i = it->first;
//             samples_processed += it->second;
            
//         	VertexSet *path = newVertexSet(1);
//         	path->vertices[0] = i;
//         	VertexSet *nbrs = newVertexSet(DG.outlist.offsets[i+1] - DG.outlist.offsets[i]);
//         	std::copy(DG.outlist.nbors+DG.outlist.offsets[i], DG.outlist.nbors+DG.outlist.offsets[i+1], nbrs->vertices);
//         	pc = newPartialClique(path, nbrs);
//         	stack->push(pc);

//             StackItem* si = stack->pop();
//             int flag = 0;
//         	EdgeIdx num_edges = 0;
//             vertex_cliques = 0;



//             while (si != NULL)  // unraveling the stack
//             {
//                 VertexSet *path = si->pc->path;
//                 VertexSet *nbrs = si->pc->nbrs;  // get the current path and its nbrs
//                 psize = path->nVertices;
//                 nsize = nbrs->nVertices;
//                 flag = 0;

//         	    k = clique_size - psize;
//                 density = get_density_and_reorder(CG, nbrs,num_edges);
        				
//                 if (psize + nsize < clique_size) 
//                 {
//                     discarded_leaves++;
//                     flag = 1;
//                 }
//         		else
//         		{
//                     double thresh_dens = 1.0 - pow((k-1), -1);
//                     if (density >= thresh_dens)
//                     {
//                         // leaves[num_leaves] = nbrs;
//                         // leafExperiments[num_leaves] = nCr[nsize][k];
//                         // leafk[num_leaves] = k;
//                         // leafn[num_leaves] = nsize;
//                         leaves.push_back(nbrs);
//                         leafExperiments.push_back(nCr[nsize][k]);
//                         leafk.push_back(k);
//                         leafn.push_back(nsize);
//                         N += nCr[nsize][k];
//                         shadow_size += nsize;

//                         num_leaves++;
//                         // if (num_leaves == max_leaves) 
//                         // {
//                         //     max_leaves += leaves_inc;
//                         //     leaves.resize(max_leaves);
//                         //     leafExperiments.resize(max_leaves);
//                         //     leafk.resize(max_leaves);
//                         //     leafn.resize(max_leaves);
//                         // }
//                     }
//                     else
//                     {
//                         internal++;
//                         for (int i=0; i<nbrs->nVertices; i++)
//                         {
//                             VertexSet *new_path = newVertexSet(1);
//                             new_path->nVertices = path->nVertices + 1; // Note: path is not really being populated
//                             VertexSet *new_nbrs = newVertexSet(nsize);

//                             int c = 0;
//                             for (int j=i+1; j<nbrs->nVertices; j++)
//                             {
//                                 if (CG.isEdge(nbrs->vertices[i], nbrs->vertices[j]) != -1)
//                                 {
//                                     new_nbrs->vertices[c] = nbrs->vertices[j];
//                                     c++;
//                                 }
//                             }
//                             new_nbrs->nVertices = c;
        			
//                             PartialClique *pc1 = newPartialClique(new_path, new_nbrs);
//                             stack->push(pc1);
//                         }

//                         flag = 1;
//                     }
//                 }
//                 if (flag == 1)
//                 {
//                     if (nbrs->nVertices >= 0) delete[] nbrs->vertices;
//                     delete nbrs;
                    
//                 }
//                 if (path->nVertices > 0) delete[] path->vertices;
//                 delete path;
//                 delete si->pc;
//                 delete si;
//                 si = stack->pop();
//             }
//             if (num_leaves != 0)
//             {
//                 std::default_random_engine generator;
//                 std::discrete_distribution<int> distribution (leafExperiments.begin(), leafExperiments.end());

//                 double leaf_cliques = 0;
//                 for (int b=0; b<it->second; b++) 
//                 {
//                     int leaf = distribution(generator);
//                     // cout << "leaf = " << leaf << endl;
//                     k = leafk[leaf];
//                     VertexSet *l = leaves[leaf];
//                     std::copy(arr, arr+l->nVertices, rng);
//                     std::random_shuffle ( rng, rng+l->nVertices);
//                     VertexIdx *sample = (VertexIdx *) calloc(k,sizeof(VertexIdx)); 
//                     // cout << "Sample number = " << b << endl;
//                     for (int j = 0; j < k; j++)
//                         sample[j] = l->vertices[rng[j]];

//                     if (isClique(sample, k, CG)) 
//                     {
//                         leaf_cliques++; //experiment was a success. Sampled set of vertices do form a clique
//                     }

//                     free(sample);
//                 }
//                 sum_of_wts += (leaf_cliques*N/degrees[i]);
//             }
 

//         }
//     	high_resolution_clock::time_point treegen_e = high_resolution_clock::now();
//         auto duration_treegen = std::chrono::duration_cast<std::chrono::microseconds>( treegen_e - treegen_s ).count();

//         double est = sum_of_wts * sigma / (double) num_samples;
//         df << est << "," <<  sum_of_wts / (double) num_samples << "," << duration_treegen << "," << endl;
//         cout << est << "," <<  sum_of_wts / (double) num_samples << "," << duration_treegen << "," << endl;
    
//         ofparams << internal << "," << num_leaves << "," << discarded_leaves << "," << shadow_size << "," << sigma << ",";
//         ofparams << duration_treegen << "," << num_samples << "," << endl;
//         ofparams << endl;
        
//     }
//     df.close();
//     ofparams.close();

//     cout << "clique_size = " << clique_size << endl;
//     cout << "internal = " << internal << endl;
//     cout << "Number of cliques counted = " << cliques_counted << endl;
//     cout << "sigma = " << sigma << endl;
// }

#endif
