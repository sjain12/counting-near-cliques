#ifndef ESCAPE_NEAR_22_CLIQUES_BRUTE_FORCE_H_
#define ESCAPE_NEAR_22_CLIQUES_BRUTE_FORCE_H_

#include <algorithm>
#include <chrono>
#include <random>
#include "Escape/ErrorCode.h"
#include "Escape/Graph.h"
#include "Escape/Digraph.h"
#include "Escape/Utils.h"
#include "Escape/nCr.h"
#include "Escape/CliqueHelper.h"
// #include "Escape/NearCliquesTuranShadow_missing_edges.h"
#include "JointSort.h"

using namespace Escape;
using namespace std;
using namespace std::chrono;

/* Algorithm to do brute force counting of cliques of size clique_size with two edges missing.
Two types possible. Type 1: both missing edges have a vertex in common. This type contains a 
clique of size cliques_size - 1. And then 1 other vertex which is connected to all but 2 of the 
clique_size - 1 vertices.

Type 2: missing edges have no vertex in common
*/
void near_22_cliques_brute_force(CGraph &CG, CDAG &DG, ofstream &of, string gname, string fname, unsigned long long clique_size)
{    
    high_resolution_clock::time_point t_elim_s = high_resolution_clock::now();
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;

    Stack* stack = newStack();
    VertexSet *path = NULL, *nbrs = NULL;
    double num_cliques = 0, num_near_cliques = 0, num_internal_nodes = 0;
    int h = clique_size - 2;

    for (VertexIdx c=0; c<CG.nVertices; c++)
    {
        path = newVertexSet(1);
        path->vertices[0] = c;
        nbrs = newVertexSet(DG.outlist.offsets[c+1] - DG.outlist.offsets[c]);
        std::copy(DG.outlist.nbors+DG.outlist.offsets[c], DG.outlist.nbors+DG.outlist.offsets[c+1], nbrs->vertices);
        
        PartialClique* pc = newPartialClique(path, nbrs);
        stack->push(pc);

        StackItem* si = stack->pop();

        while (si != NULL)
        {
            path = si->pc->path;
            nbrs = si->pc->nbrs;  // get the current path and its nbrs
            VertexIdx psize = path->nVertices, nsize = nbrs->nVertices;
            int flag = 0;
            if (psize + nsize < h)
            {
            }
            else if (psize == h)
            {   

                // VertexSet *clique = newVertexSet(clique_size);
                // std::copy(sample_path->vertices, sample_path->vertices+(clique_size-k), clique->vertices);
                // std::copy(sample, sample+k, clique->vertices+(clique_size-k));

                // num_near_cliques += get_num_partial_nbrs(CG, path, 2);

                // for u,w in clique, u<w, find all nbrsu (x) such that they are connected to all in clique except w and w < x
                // find all nbrsw (v) of w that are connected to all in clique but u
                // if (x,w) connected, near_clique++
                num_near_cliques += get_num_22_cliques(CG, path);
            }
            else
            {
                num_internal_nodes++;

                for (VertexIdx i=0; i<nsize; i++)
                {
                    VertexIdx v = nbrs->vertices[i];
                    VertexSet *new_path = newVertexSet(psize+1);
                    std::copy(path->vertices, path->vertices+path->nVertices, new_path->vertices);
                    new_path->vertices[psize] = v;

                    VertexSet *new_nbrs = newVertexSet(nsize-1-i);
                    int k = 0;
                    for (int j=i+1; j<nsize; j++)
                    {
                        if (CG.isEdge(v, nbrs->vertices[j]) != -1)
                        {
                            new_nbrs->vertices[k] = nbrs->vertices[j];
                            k++;
                        }
                    }
                    if (((psize+1)<h) && (k == 0))
                    {
                        delete[] new_nbrs->vertices;
                        delete new_nbrs;
                        delete[] new_path->vertices;
                        delete new_path;
                        continue;
                    }
                    else
                    {
                        new_nbrs->nVertices = k; 
                        PartialClique *pc = newPartialClique(new_path, new_nbrs);
                        stack->push(pc);
                    }
                }
            }
            
        	if (nbrs->nVertices >= 0) delete[] nbrs->vertices;
            delete nbrs;
            if (path->nVertices >= 0) delete[] path->vertices;
            delete path;
            delete si->pc;
            delete si;
            si = stack->pop();
        }
    }

    delStack(stack);

    high_resolution_clock::time_point t_elim_e = high_resolution_clock::now();
    auto duration_program = std::chrono::duration_cast<std::chrono::microseconds>( t_elim_e - t_elim_s ).count();
    
    cout << "time reqd: " << duration_program << endl;
    cout << "number of near cliques = " << num_near_cliques << endl;
    cout << "number of num_internal_nodes = " << num_internal_nodes << endl;
	cout << "near-clique size = " << clique_size << endl;
    std::cout << "currentDateTime()=" << currentDateTime() << std::endl;


	of << clique_size << "," << num_near_cliques << "," << duration_program << "," << currentDateTime() << endl;
}

#endif
