# import numpy as np
# import matplotlib.pyplot as pl
# import pandas as pd
# graphs = ['ca-AstroPh','com-youtube','dblp_coauthor']

# df = pd.DataFrame() #pd.read_csv('amazon0601_1000000_5_0_data')

# df_all_params = pd.read_csv('allparams1000000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_all_data = pd.read_csv('alldata1000000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_params = pd.read_csv('bruteforceparams.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])


# x = range(5,11)
# pl.figure(1)
# gb = df_all_params.groupby('gname')
# gb.unstack(level=0).plot(kind='line', subplots=True)
# # df_all_params['             leaves'].plot(x='csize')
# pl.show()
# # for g in graphs:




# coding: utf-8

# In[42]:

# get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

# graphs = ['ca-AstroPh','cit-HepPh','com-youtube','dblp_coauthor','amazon0601', 'as-skitter','facebook_combined','patentcite','soc-pokec-relationships', 'web-BerkStan','web-Google','web-Stanford'] #,'orkut-links']
graphs = ['loc-gowalla_edges', 'web-Stanford', 'amazon0601','com-youtube','web-Google','web-BerkStan','as-skitter','cit-Patents','soc-pokec-relationships','com-lj','com-orkut']

# graphs = ['amazon0601']
df_stats = pd.read_csv('GraphStats_new_graphs_correct.csv', sep=',', index_col=[0]) #, usecols=[0,1])
df_all_params = pd.read_csv('allparams50000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
df_all_data = pd.read_csv('alldata50000.csv', sep=',', index_col=[0,1,2]) #, usecols=[0,1])
# df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_params = pd.read_csv('bruteforceparams.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
print df_all_params
# fig, ax = pl.subplots()
x = df_stats.ix[:,1:2]
print x
# y = df_all_data.unstack(level=)
# print x
for i in range(6):
	fig, ax = pl.subplots()
	x = []
	y = []
	print 'hello'
	for g in graphs:
		x.append(df_stats.ix[g,1:2])
		# print df_all_params.ix[g,1:2].ix[i+5]
		y.append(df_all_params.ix[g,1:2].ix[i+5])
	print x
	print y
	ax.set_xscale("log")
	ax.set_yscale("log")
	pl.gcf().subplots_adjust(bottom=0.15)                                                               
	pl.rcParams.update({'font.size': 22}) 
	pl.scatter(x,y,cmap=pl.cm.coolwarm)
	pl.ylim(ymin=1)
	pl.xlim(xmin=1)
	lims = [
    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
	]

	# now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	
	pl.plot()
	# for g in graphs:
	#     ax.annotate(g, (df_stats.ix[g,1:2],df_all_params.ix[g,1:2].ix[i+5]))

	ax.set_title('Number of leaves vs. number of edges for k='+str(i+5))
	ttl = ax.title
	ttl.set_position([.5, 1.05])
	ax.set_ylabel('Number of leaves')
	ax.set_xlabel('Number of edges')
	pl.savefig('plots/leaves_vs_edges_k='+str(i+5)+'.pdf', format='pdf')
	pl.show()
	print x
	print y




	# print df_all_data.ix[g,1:2].unstack(level=0).ix[0,3]
# 	fig, ax = pl.subplots()
# 	print df_bf_data.ix[g, 1:2]
# 	print df_all_data.ix[g,1:2]
# 	# ax.plot(range(5,11), df_bf_params.ix[g, 0:3].sum(axis=1), label='brute_force')
# 	ax.plot(range(5,11), df_all_params.ix[g,1:2], label='clique_estimate')
# 	legend = ax.legend(loc='upper right', shadow=True)
# 	ax.set_title(g)
# 	ax.set_xlabel('clique size')
# 	ax.set_ylabel('number of nodes')
# 	pl.savefig('plots/'+g+'_bf_est_num_nodes')
# 	pl.show()


# for g in graphs:
# 	fig, ax = pl.subplots()
# 	print df_bf_data.ix[g, 0:1]
# 	print df_all_data.ix[g,0:1]
# 	ax.plot(range(5,11), df_bf_data.ix[g, 0:1], label='brute_force')
# 	ax.plot(range(5,11), df_all_data.ix[g,0:1], label='clique_estimate')
# 	legend = ax.legend(loc='upper right', shadow=True)
# 	ax.set_title(g)
# 	ax.set_xlabel('clique size')
# 	ax.set_ylabel('number of cliques')
# 	pl.show()
# 	pl.savefig('plots/'+g+'_bf_est_num_cliques')




# print df_bf_params.ix['ca-AstroPh',1:2].plot(legend=False)
# # print df_bf_params.ix[:'ca-AstroPh',1:2].unstack(level=0)
# print df_all_params.ix['ca-AstroPh',1:2].plot(legend=False)
# df_bf_params.ix[:'ca-AstroPh',1:2].unstack(level=0).plot(legend=False)
# df_all_params.sort_index()
# df_all_params.ix[:'ca-AstroPh',1:2].unstack(level=0).plot(legend=False)

# df_all_params.unstack(level=0).plot(legend=False)
# ax = df_bf_params.ix[:'ca-AstroPh',1:2].plot()
# df_bf_params.ix[:'com-youtube',1:2].plot()
# df_bf_params.ix[:'dblp_coauthor',1:2].plot()







