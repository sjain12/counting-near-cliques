import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

pl.rcParams['text.usetex'] = True

graphs = ['loc-gowalla_edges', 'web-Stanford', 'amazon0601','com-youtube','web-Google','web-BerkStan','as-skitter','cit-Patents','soc-pokec-relationships','com-lj','com-orkut']

for k in [7]:
	df_timings = pd.read_csv('timings_'+str(k)+'.csv', sep=',', index_col=[0]) #, usecols=[0,1])
	df_timings['CC'] = df_timings['CC'] / df_timings['TS']
	df_timings['ES'] = df_timings['ES'] / df_timings['TS']
	df_timings['GRAFT'] = df_timings['GRAFT'] / df_timings['TS']
	print df_timings
	fig, ax = pl.subplots()
	# pl.gcf().subplots_adjust(bottom=0.08)                                                               
	pl.rcParams.update({'font.size': 22}) 
	index = np.arange(len(graphs))+0.5
	bar_width = 0.2
	opacity = 0.8
	ax.set_yscale("log")
	names = ['loc-gow', 'web-Stan', 'amazon','youtube','Google','BerkStan','as-skitter','Patents','soc-pokec','com-lj','com-orkut']
	# df_timings.set_index(names, inplace='true')
	# rects1 = pl.bar(index, df_timings['CC'], bar_width,
	#                  alpha=opacity,
	#                  color='orange',
	#                  label='CC', edgecolor = "orange")
	 
	rects2 = pl.bar(index , df_timings['ES'], bar_width,
	                 alpha=opacity,
	                 color='tomato',
	                 label='ES', edgecolor = "tomato")
	rects3 = pl.bar(index + (1*bar_width), df_timings['GRAFT'], bar_width,
	                 alpha=opacity,
	                 color='green',
	                 label='GRAFT', edgecolor = "green")

	# rects4 = pl.bar(index + (3*bar_width), df_timings['TS'], bar_width,
	#                  alpha=opacity,
	#                  color='m',
	#                  label='TS')
	# pl.xticks(index + bar_width, ('A', 'B', 'C', 'D'))
	# if k==7:
	# 	ax.set_ylim(ymax=20000)
	# else:
	# 	ax.set_ylim(ymax=30000)
	
	#pl.legend(loc=1)
	# ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
 #          ncol=2)
	# ax.legend(loc=9) #, bbox_to_anchor=(0.5, 1.1),
          #ncol=2)
	# barlist[0].set_color('r')
	#pl.xticks(index+1, graphs)
	pl.xticks(index + (1.5*bar_width), df_timings.index, rotation='vertical') #graphs)
	ax.set_xticklabels(names)
	
	pl.legend(loc=9,
           ncol=3, mode="expand", borderaxespad=0.,prop={'size':22}, frameon=False)
	[xm, xM] = ax.get_xlim()
	# xM = ax.get_xmax()
	pl.plot([xm,xM], [1]*2, color='crimson')
	ax.set_xlim(xmin=0, xmax=11.5)
	# pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
	ax.set_title('k='+str(k))
	ttl = ax.title
	ttl.set_position([.5, 1.05])
	ax.set_ylabel('Speedup')

	ax.set_xlabel('Graphs')
	pl.tight_layout()
	pl.savefig('plots/speedup_k='+str(k)+'.pdf', format='pdf')
	pl.show()


graphs = ['web-BerkStan','as-skitter','com-lj','com-orkut']

for k in [10]:
	df_timings = pd.read_csv('timings_'+str(k)+'.csv', sep=',', index_col=[0]) #, usecols=[0,1])
	df_timings['CC'] = df_timings['CC'] / df_timings['TS']
	df_timings['ES'] = df_timings['ES'] / df_timings['TS']
	df_timings['GRAFT'] = df_timings['GRAFT'] / df_timings['TS']


	fig, ax = pl.subplots()
	# pl.gcf().subplots_adjust(bottom=0.08)                                                               
	pl.rcParams.update({'font.size': 22}) 
	index = np.arange(len(graphs))+0.5
	bar_width = 0.2
	opacity = 0.8
	ax.set_yscale("log")

	# fig, ax = pl.subplots()
	# pl.gcf().subplots_adjust(bottom=0.10)                                                               
	# pl.rcParams.update({'font.size': 20}) 
	# index = np.arange(4)+1
	# bar_width = 0.15
	# opacity = 1
	names = ['web-Berk', 'as-skitter', 'com-lj','com-orkut']

	# pl.show()
	rects1 = pl.bar(index, df_timings['CC'], bar_width,
	                 alpha=opacity,
	                 color='orange',
	                 label='CC', edgecolor = "orange")
	 
	rects2 = pl.bar(index + (1*bar_width), df_timings['ES'], bar_width,
	                 alpha=opacity,
	                 color='mediumpurple',
	                 label='ES', edgecolor = "mediumpurple")
	rects3 = pl.bar(index + (2*bar_width), df_timings['GRAFT'], bar_width,
	                 alpha=opacity,
	                 color='lightgreen',
	                 label='GRAFT', edgecolor = "lightgreen")

	pl.xticks(index + (1.5*bar_width), df_timings.index, rotation='vertical') #graphs)
	ax.set_xticklabels(names)
	ax.set_xlim(xmin=0, xmax=4.5)
	pl.legend(loc=9,
           ncol=3, mode="expand", borderaxespad=0.,prop={'size':22}, frameon=False)
	[xm, xM] = ax.get_xlim()
	ax.set_ylim(ymin=0.1)
	# xM = ax.get_xmax()
	pl.plot([0,4.5], [1]*2, color='tomato')
	# ax.set_xlim(xmin=0, xmax=11.5)
	# pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
	ax.set_title('k='+str(k))
	ttl = ax.title
	ttl.set_position([.5, 1.05])
	ax.set_ylabel('Speedup')

	ax.set_xlabel('Graphs')
	pl.tight_layout()
	pl.savefig('plots/speedup_k='+str(k)+'.pdf', format='pdf')
	pl.show()
	# pl.xticks(index + bar_width, ('A', 'B', 'C', 'D'))
	# if k==7:
	# 	ax.set_ylim(ymax=20000)
	# else:
	# 	ax.set_ylim(ymax=30000)
	
	#pl.legend(loc=1)
	# ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
 #          ncol=2)
	# # barlist[0].set_color('r')
	# #pl.xticks(index+1, graphs)
	# pl.xticks(index + (2*bar_width), df_timings.index) #graphs)
	# # pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
	# ax.set_title('Timings for k='+str(k))
	# ttl = ax.title
	# ttl.set_position([.5, 1.05])
	# ax.set_ylabel('Time in seconds')

	# ax.set_xlabel('Graphs')
	# pl.tight_layout()
	# pl.savefig('plots/timings_k='+str(k)+'.pdf', format='pdf')
	# pl.show()