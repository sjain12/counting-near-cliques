import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.rand(3,4), columns=list("ABCD"))
pd.set_option('float_format', '{:.2e}'.format)
df.to_csv('temp.csv', float_format='%.2e')

print df
