# import numpy as np
# import matplotlib.pyplot as pl
# import pandas as pd
# graphs = ['ca-AstroPh','com-youtube','dblp_coauthor']

# df = pd.DataFrame() #pd.read_csv('amazon0601_1000000_5_0_data')

# df_all_params = pd.read_csv('allparams1000000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_all_data = pd.read_csv('alldata1000000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_params = pd.read_csv('bruteforceparams.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])


# x = range(5,11)
# pl.figure(1)
# gb = df_all_params.groupby('gname')
# gb.unstack(level=0).plot(kind='line', subplots=True)
# # df_all_params['             leaves'].plot(x='csize')
# pl.show()
# # for g in graphs:




# coding: utf-8

# In[42]:

# get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

pl.rcParams['text.usetex'] = True

# graphs = ['ca-AstroPh','com-youtube','dblp_coauthor','cit-HepPh','amazon0601']
# graphs = ['as-skitter','facebook_combined','patentcite','soc-pokec-relationships', 'web-BerkStan','web-Google','web-Stanford'] #,'orkut-links']
graphs = ['amazon0601','com-youtube','web-BerkStan','as-skitter','cit-Patents','com-lj','com-orkut']
color = {}
color['com-youtube'] = 'r'
color['web-BerkStan'] = 'b'
color['as-skitter'] = 'g'
color['cit-Patents'] = 'm'
color['amazon0601'] = 'c'
color['com-lj'] = 'k'
color['com-orkut'] = 'y'
df_all_params = pd.read_csv('allparams50000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
df_all_data = pd.read_csv('alldata50000.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
print df_all_data
# df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
# df_bf_params = pd.read_csv('bruteforceparams.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])



# print 'hello'
# for g in graphs:
# 	fig, ax = pl.subplots()
# 	ax.plot(range(5,11), df_bf_params.ix[g, 1:2], label='brute_force')
# 	ax.plot(range(5,11), df_all_params.ix[g,1:2], label='clique_estimate')
# 	legend = ax.legend(loc='upper right', shadow=True)
# 	ax.set_title(g)
# 	ax.set_xlabel('clique size')
# 	ax.set_ylabel('number of leaves')
# 	pl.show()
# 	pl.savefig('plots/'+g+'_bf_est_num_leaves')
fig, ax = pl.subplots(figsize=(8,6))
pl.gcf().subplots_adjust(bottom=0.10)                                                               
pl.rcParams.update({'font.size': 16}) 
for g in graphs:
	ax.set_yscale("log")
	# print df_all_data.ix[g,0:1].unstack(level=0).ix[0]
	# ax.plot(range(5,11), df_bf_data.ix[g, 0:1], label='brute_force',color='red')
	label = g
	if label == 'soc-pokec-relationships':
		label = 'soc-pokec'
	# print df_all_data.ix[g]['estimate']
	# locator = pl.MaxNLocator(nbins=5) # with 3 bins you will have 4 ticks
	# ax.yaxis.set_major_locator(locator)
	ax.plot(range(5,11), df_all_data.ix[g]['estimate'], '-x', label=label, color=color[g]) # df_all_data.ix[g,0:1].unstack(level=0).ix[0]

ax.set_position([0.12,0.1,0.5,0.6])

legend = ax.legend(loc='center left', bbox_to_anchor = (1, 0.6))
# pl.legend(loc=(1.1,0.5))

ax.set_title('Trends in clique counts')
ttl = ax.title
ttl.set_position([.5, 1.05])
ax.set_xlabel('Clique size')
ax.set_ylim(ymin=1)
ax.set_ylabel('Cliques')
# pl.tight_layout()
fig.set_size_inches(8, 6)
pl.savefig('plots/est_num_cliques_log.pdf', format='pdf', bbox_inches='tight', alpha=0.0)
pl.show()





# print df_bf_params.ix['ca-AstroPh',1:2].plot(legend=False)
# # print df_bf_params.ix[:'ca-AstroPh',1:2].unstack(level=0)
# print df_all_params.ix['ca-AstroPh',1:2].plot(legend=False)
# df_bf_params.ix[:'ca-AstroPh',1:2].unstack(level=0).plot(legend=False)
# df_all_params.sort_index()
# df_all_params.ix[:'ca-AstroPh',1:2].unstack(level=0).plot(legend=False)

# df_all_params.unstack(level=0).plot(legend=False)
# ax = df_bf_params.ix[:'ca-AstroPh',1:2].plot()
# df_bf_params.ix[:'com-youtube',1:2].plot()
# df_bf_params.ix[:'dblp_coauthor',1:2].plot()







