import numpy as np
import matplotlib.pyplot as pl
import pandas as pd


# graphs = ['as-skitter','amazon0601','cit-Patents','web-Google','com-orkut','wiki-Talk','soc-LiveJournal1','soc-pokec-relationships','PaperReferences']
graphs = ['loc-gowalla_edges', 'web-Stanford', 'amazon0601', 'com-youtube', 'web-Google','web-BerkStan', 'as-skitter', 'cit-Patents', 'soc-pokec-relationships', 'com-lj', 'com-orkut'] #,'orkut-links'] 'soc-LiveJournal1',,

# graphs = ['as-skitter','amazon0601']

def get_est_data():
	dfall = pd.DataFrame() #pd.read_csv('amazon0601_1000000_5_0_data')

	for g in graphs:
		print 'yo'
		print g
		fname_5 = g +'_50000_5_1_0_data' 
		fname_6 = g +'_50000_6_1_0_data' 
		fname_7 = g +'_50000_7_1_0_data' 
		fname_8 = g +'_50000_8_1_0_data' 
		fname_9 = g +'_50000_9_1_0_data' 
		fname_10 = g +'_50000_10_1_0_data' 

		
		df_5 = pd.read_csv(fname_5, sep=',', index_col=False, usecols=[0,1,2])
		df_6 = pd.read_csv(fname_6, sep=',', index_col=False, usecols=[0,1,2])
		df_7 = pd.read_csv(fname_7, sep=',', index_col=False, usecols=[0,1,2])
		df_8 = pd.read_csv(fname_8, sep=',', index_col=False, usecols=[0,1,2])
		df_9 = pd.read_csv(fname_9, sep=',', index_col=False, usecols=[0,1,2])
		df_10 = pd.read_csv(fname_10, sep=',', index_col=False, usecols=[0,1,2])
		# if (g == 'loc-gowalla_edges'):
		# 	g = 'loc-gowalla'
		# if (g == 'soc-pokec-relationships'):
		# 	g = 
		df_5['gname'] = g
		df_5['csize'] = 5
		df_6['gname'] = g
		df_6['csize'] = 6
		df_7['gname'] = g
		df_7['csize'] = 7
		df_8['gname'] = g
		df_8['csize'] = 8
		df_9['gname'] = g
		df_9['csize'] = 9
		df_10['gname'] = g
		df_10['csize'] = 10

		# df_5['run'] = range(20)
		# df_6['run'] = range(20)
		# df_7['run'] = range(20)
		# df_8['run'] = range(20)
		# df_9['run'] = range(20)
		# df_10['run'] = range(20)
		
		
		df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10])#, ignore_index=True)

		# print df 
		# df = df['gname','csize','estimate','succ_ratio']
		cols = df.columns.tolist()
		cols = cols[-1:] + cols[:-1]
		
		cols = cols[-1:] + cols[:-1]

		cols = cols[-1:] + cols[:-1]
		df = df[cols]

		# print df
		dfall = dfall.append(df)
		indexed_df_data = dfall.set_index(['gname','csize']) #,'run'])
		# print indexed_df_data

		indexed_df_data.to_csv('alldata50000.csv')

	return indexed_df_data



def get_est_params():
	df_params = pd.DataFrame()

	for g in graphs:
		print 'yo'
		fname_5 = g +'_50000_5_0_params' 
		fname_6 = g +'_50000_6_0_params' 
		fname_7 = g +'_50000_7_0_params' 
		fname_8 = g +'_50000_8_0_params' 
		fname_9 = g +'_50000_9_0_params' 
		fname_10 = g +'_50000_10_0_params' 
		
		df_5 = pd.read_csv(fname_5, sep=',', index_col=False, usecols=range(0,7), nrows=1)
		df_6 = pd.read_csv(fname_6, sep=',', index_col=False, usecols=range(0,7), nrows=1)
		df_7 = pd.read_csv(fname_7, sep=',', index_col=False, usecols=range(0,7), nrows=1)
		df_8 = pd.read_csv(fname_8, sep=',', index_col=False, usecols=range(0,7), nrows=1)
		df_9 = pd.read_csv(fname_9, sep=',', index_col=False, usecols=range(0,7), nrows=1)
		df_10 = pd.read_csv(fname_10, sep=',', index_col=False, usecols=range(0,7), nrows=1)

		df_5['gname'] = g
		df_5['csize'] = 5
		df_6['gname'] = g
		df_6['csize'] = 6
		df_7['gname'] = g
		df_7['csize'] = 7
		df_8['gname'] = g
		df_8['csize'] = 8
		df_9['gname'] = g
		df_9['csize'] = 9
		df_10['gname'] = g
		df_10['csize'] = 10


		# x=[(g,i) for i in range(5,11,1)]

		# df = pd.DataFrame(x,columns=['gname','csize'])
		# print df
		
		df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10]) #, ignore_index=True)

		# df = df['gname','csize','estimate','succ_ratio']
		cols = df.columns.tolist()
		print cols
		cols = cols[-1:] + cols[:-1]
		
		cols = cols[-1:] + cols[:-1]

		df = df[cols]

		# print df
		df_params = df_params.append(df)
		indexed_df_params = df_params.set_index(['gname','csize'])
		# print indexed_df_params

		indexed_df_params.to_csv('allparams50000.csv')

	return indexed_df_params


	# indexed_df = df.set_index(['gname','csize'])

dfdata = get_est_data()

dfparams = get_est_params()
df = pd.DataFrame()
df = dfdata.ix[:,1:2].astype(float)
print ('got df')
print df
df['time_tree'] = dfparams.ix[:,5:6]
df['sample_time'] = dfdata.ix[:,0:1]
df['time_sec'] = np.ceil((df['time_tree'] + df['sample_time']) / 1000000)
df['time'] = df['time_sec']
print('set time')
print df
# df = df[[0,4,5]]
# print df.unstack(level=1)
# np.savetxt('est_table_abridged.csv',df.unstack(level=1),fmt='%1.2e',delimiter=',')

# pd.to_datetime(df['time_sec'], unit='s').dt.time
# str(timedelta(microseconds=49503757))

# df['time_minutes'] = np.floor(df['time_micro'] / 60000000)
# df['time_seconds'] = np.ceil((df['time_micro'] - (60000000 * df['time_minutes'])) / 60)
df['error'] = 0
print df.columns
df.columns = ['estimate', 'time_tree', 'sample_time', 'time_sec', 'time', 'error']
df['estimate'] = df['estimate'].map('{:.2e}'.format)

# print df.columns
# print df

df = df[[0,4,5]]
df.to_csv('est_table_all.csv')
print df
stacked = df.unstack(level=1)
df = stacked 

# stacked = df.stack()
# df = stacked.unstack(1)
# # df1 = df.ix[:,:].unstack(level=1)
# # df.unstack(level=0)
# # pd.set_option('float_format', '{:.2e}'.format)
print df.columns
print df.index

# df['estimate'].map('${:.2e}'.format)
df.to_csv('est_table_abridged.csv') #, float_format='%.2e')
# print df.ix[:,3:4] #.map('${:.2f}'.format)

print df
# np.savetxt('est_table.csv',df,fmt='%1.3f',delimiter=',')
