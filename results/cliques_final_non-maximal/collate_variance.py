import numpy as np
import matplotlib.pyplot as pl
import matplotlib as mpl
import pandas as pd
import copy
import os

# graphs = ['as-skitter','amazon0601','cit-Patents','web-Google','com-orkut','wiki-Talk','soc-LiveJournal1','soc-pokec-relationships','PaperReferences']
graphs = ['amazon0601','web-Google'] #,'orkut-links'] #'soc-LiveJournal1',,'com-lj',

# graphs = ['as-skitter','amazon0601']


df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1])

#get table of cc
# get table of es1
# get table of es2
# get table of bf 
# get table of ours

def get_est_cc(g,c):
	fname = g +'_' + str(c) + '_100_cc' 
	# if (os.path.exists(fname)):
	df_cc = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
	# else:
	# 	df_cc = pd.read_csv('dummy_cc', sep=',', index_col=False, usecols=[0,1,2])
	return df_cc

def get_est_es2(g, c, n):
	fname = g +'_' + str(n) + '_' + str(c) + '_100_es2' 
	df_es2 = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2]) #, skiprows=1)
	return df_es2

def get_est_es1(g, c, p):
	fname = g + '_' + str(c) + '_' + p +'_100_es1' 
	df_es1 = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2,3,4], skiprows=0)
	return df_es1

def get_est_data(g, c, n):
	fname = g +'_' + str(n) + '_' + str(c) + '_100_0_data' 
	dfdata = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
	fname = g +'_' + str(n) + '_' + str(c) + '_0_params' 
	dfparams = pd.read_csv(fname, sep=',', index_col=False, usecols=range(0,7), nrows=1)
	# print dfdata['time'] + dfparams['tree_t']
	return dfdata
	# df = dfdata.ix[:,1:2]
	# df['time_tree'] = dfparams.ix[:,5:6]
	# df['sample_time'] = dfdata.ix[:,0:1]
	# df['time_sec'] = df['time_tree'] + df['sample_time']

	# return df_est
actual = {}
actual['amazon0601'] = 988617
actual['web-Google'] = 6.0547e+08


for g in graphs:
	# df_cc = get_est_cc(g,7)
	# print df_cc
	df_cc = get_est_cc(g,7)
	df_es2 = get_est_es2(g,7,100000)
	df_es1 = get_est_es1(g,7,'0.75')
	print df_es1
	df_data = get_est_data(g,7,50000)
	df = pd.DataFrame()
	df['CC'] = df_cc['estimate']
	df['GUISE'] = df_es1['estimate']
	df['GRAFT'] = df_es2['estimate']
	df['TS'] = df_data['estimate']
	print df
	fig = pl.figure() 
	pl.gcf().subplots_adjust(bottom=0.15)                                                               
	pl.rcParams.update({'font.size': 22})                                          
	ax = fig.add_subplot(1,1,1) 
	pl.scatter([1] * 100, df['CC'],color='blue')
	pl.scatter([2] * 100, df['GUISE'],color='blue')
	pl.scatter([3] * 100, df['GRAFT'],color='blue')
	pl.scatter([4] * 100, df['TS'],color='blue')
	ax.set_title(g+' for k=7')
	ttl = ax.title
	ttl.set_position([.5, 1.05])
	ax.set_ylim(ymin=0)
	ax.set_ylabel('Estimates')
	ax.set_xlabel('Algorithms')
	pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

	pl.savefig('plots/'+g+'_7_all_estimates_scatter.pdf', format='pdf')
	pl.show()

	fig = pl.figure()  
	pl.gcf().subplots_adjust(bottom=0.15)                                                               
                                                             
	ax = fig.add_subplot(1,1,1) 

	df = (df - actual[g]) * 100 / actual[g]
	pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
	pl.rcParams.update({'font.size': 20})
	df.boxplot(ax=ax)
	ax.set_title(g+' for k=7')
	ttl = ax.title
	ttl.set_position([.5, 1.05])
	ax.set_ylabel('% error')
	ax.set_xlabel('Algorithms')

	pl.ylim([-10, 10])
	pl.savefig('plots/'+g+'_7_error_box.pdf', format='pdf')

	pl.show()




		
















# 	dfall = pd.DataFrame() #pd.read_csv('amazon0601_1000000_5_0_data')
# 	# num_samples = [10000, 50000, 100000, 500000, 1000000]
# 	for g in graphs:
# 		# pring g
# 		# for c in range(5,11):
# 			# fig, ax = pl.subplots()
# 			# fig = pl.figure()                                                               
# 			# ax = fig.add_subplot(1,1,1) 
# 			# pl.xlim([1,xmax])
# 			for n in num_samples:
# 				print 'yo'
# 				print g
# 				fname = g +'_'+str(n)+'_' + str(c) + '_100_0_data' 
# 				df = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
# 				pl.scatter([n] * 100, df.ix[:, 0:1],color='blue')
# 				#ax.plot([n] * 100, df.ix[:, 0:1],color='blue')
# 			actual = df_bf_data.ix[g,0:1].iloc[c-5].item()
# 			print actual
# 			xmax = 1010000
# 			# pl.xlim(xmin=0, xmax=xmax)
# 			ax.plot([0,xmax], [actual] * 2, color='red')
# 			ax.set_xscale("log")
# 			xtics = copy.deepcopy(num_samples)
# 			xtics[:0] = [0]
 
# 			print xtics
# 			ax.set_xticks(xtics)
# 			# ax.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter())
			
# 			ymax = 1.4*actual
# 			print ymax
# 			pl.xlim([1,xmax])
# 			pl.ylim([0,ymax])
# 			ax.set_title('Estimate vs num of samples for k='+str(c))
# 			ax.set_ylabel('number of cliques')
# 			ax.set_xlabel('number of samples')

# 			# axes = pl.gca()
# 			# axes.set_xlim([0,xmax])
# 			# axes.set_ylim([0,ymax])
# 			pl.savefig('plots/'+g+'_'+str(c)+'_convergence')
# 			pl.show()

# get_est_data_convergence()

# 				# fname_6 = g +'_100000_6_100_0_data' 
# 				# fname_7 = g +'_100000_7_100_0_data' 
# 				# fname_8 = g +'_100000_8_100_0_data' 
# 				# fname_9 = g +'_100000_9_100_0_data' 
# 				# fname_10 = g +'_100000_10_100_0_data' 
			
# 			# df_5['gname'] = g
			# df_5['csize'] = 5
			# df_6['gname'] = g
			# df_6['csize'] = 6
			# df_7['gname'] = g
			# df_7['csize'] = 7
			# df_8['gname'] = g
			# df_8['csize'] = 8
			# df_9['gname'] = g
			# df_9['csize'] = 9
			# df_10['gname'] = g
			# df_10['csize'] = 10

			# df_5['run'] = range(100)
			# df_6['run'] = range(100)
			# df_7['run'] = range(100)
			# df_8['run'] = range(100)
			# df_9['run'] = range(100)
			# df_10['run'] = range(100)
			
			
		# 	df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10], ignore_index=True)

		# 	# print df 
		# 	# df = df['gname','csize','estimate','succ_ratio']
		# 	cols = df.columns.tolist()
		# 	cols = cols[-1:] + cols[:-1]
			
		# 	cols = cols[-1:] + cols[:-1]

		# 	cols = cols[-1:] + cols[:-1]
		# 	df = df[cols]

		# 	# print df
		# 	dfall = dfall.append(df)
		# 	indexed_df_data = dfall.set_index(['gname','csize']) #,'run'])
		# 	# print indexed_df_data

		# 	indexed_df_data.to_csv('alldata100000.csv')

		# return indexed_df_data