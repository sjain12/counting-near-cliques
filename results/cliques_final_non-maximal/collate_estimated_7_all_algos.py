import numpy as np
import matplotlib.pyplot as pl
import pandas as pd
import os


# graphs = ['as-skitter','amazon0601','cit-Patents','web-Google','com-orkut','wiki-Talk','soc-LiveJournal1','soc-pokec-relationships','PaperReferences']
graphs = ['loc-gowalla_edges', 'web-Stanford', 'amazon0601', 'com-youtube', 'web-Google','web-BerkStan', 'as-skitter', 'cit-Patents', 'soc-pokec-relationships', 'com-lj', 'com-orkut', 'soc-LiveJournal1', 'gplus_combined']

# graphs = ['as-skitter','amazon0601']

def get_est_bf(g,c):
	fname = g +'_' + str(c) + '_1_bf' 
	if (os.path.exists(fname)):
		df_bf = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
	else:

		df_bf = pd.read_csv('dummy_bf', sep=',', index_col=False, usecols=[0,1,2])
	return df_bf

def get_est_cc(g,c):
	fname = g +'_' + str(c) + '_1_cc' 
	if (os.path.exists(fname)):
		df_cc = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
	else:
		df_cc = pd.read_csv('dummy_cc', sep=',', index_col=False, usecols=[0,1,2])
	return df_cc

def get_est_es2(g, c, n):
	fname = g +'_' + str(n) + '_' + str(c) + '_1_es2_temp' 
	if (os.path.exists(fname)):
		df_es2 = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2]) #, skiprows=1)
	else:
		print 'calling dummy for ' + fname
		df_es2 = pd.read_csv('dummy_es2', sep=',', index_col=False, usecols=[0,1,2])
	return df_es2

def get_est_es1(g, c, p):
	fname = g + '_' + str(c) + '_' + p +'_1_es1' 
	if (os.path.exists(fname)):
		df_es1 = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2,3,4], skiprows=0)
	else:
		df_es1 = pd.read_csv('dummy_es1', sep=',', index_col=False, usecols=[0,1,2,3,4], skiprows=0)
	return df_es1

def get_est_data(g, c, n):
	fname = g +'_' + str(n) + '_' + str(c) + '_1_0_data' 
	if (os.path.exists(fname)):
		dfdata = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
	else:
		dfdata = pd.read_csv('dummy_data', sep=',', index_col=False, usecols=[0,1,2])

	fname = g +'_' + str(n) + '_' + str(c) + '_0_params' 
	if (os.path.exists(fname)):
		dfparams = pd.read_csv(fname, sep=',', index_col=False, usecols=range(0,7), nrows=1)
	else:
		dfparams = pd.read_csv('dummy_params', sep=',', index_col=False, usecols=range(0,7), nrows=1)
	dfdata['time'] = dfdata['time'] + dfparams['tree_t']
	# print dfdata['time'] + dfparams['tree_t']
	return dfdata

actual = {}

# df_all.columns = ['graph', 'cc estimate', 'ds estimate', 'es estimate', 'LS estimate','cc time','ds time','es time', 'LS time']
def get_est_all_algos():
	df_all = pd.DataFrame()
	for g in graphs:
		print g
		df_cc = get_est_cc(g,7)
		df_bf = get_est_bf(g,7)
		df_es2 = get_est_es2(g,7,100000)
		df_es1 = get_est_es1(g,7,'0.50')
		df_es11 = get_est_es1(g,7,'0.75')
		df_data = get_est_data(g,7,50000)
		
		df = pd.DataFrame()
		
		# print df.ix[0:1].iloc[0].item()
		# df['graph'].astype(object)
		# df['graph'] = df['graph'].apply(lambda x: str(x))
		#ix[g,0:1].iloc[c-5].item()
		
		df['CC estimate'] = df_cc['estimate']
		
		df['DS p=0.5'] = df_es1['estimate']
		df['DS p=0.75'] = df_es11['estimate']

		df['ES estimate'] = df_es2['estimate']
		
		df['LS estimate'] = df_data['estimate']
		df['BF'] = df_bf['estimate']
		df['CC time'] = df_cc['time']
		df['DS p=0.5 time'] = df_es1['time']
		df['DS p=0.75 time'] = df_es11['time']
		df['ES time'] = df_es2['time']
		df['LS time'] = df_data['time']
		df['BF time'] = df_bf['time']
		df['graph'] = g
		print df['graph']
		print df
		df_all = df_all.append(df)
	cols = df_all.columns.tolist()
	cols = cols[-1:] + cols[:-1]
	df_all = df_all[cols]
	df_all.set_index(['graph'])
	print df_all[1:12]
	return df_all

df_all = get_est_all_algos()
df_all.to_csv('all_estimates.csv')
# 	for g in graphs:
# 		print 'yo'
# 		print g
# 		fname_cc = g +'_50000_5_1_0_data' 
# 		fname_6 = g +'_50000_6_1_0_data' 
# 		fname_7 = g +'_50000_7_1_0_data' 
# 		fname_8 = g +'_50000_8_1_0_data' 
# 		fname_9 = g +'_50000_9_1_0_data' 
# 		fname_10 = g +'_50000_10_1_0_data' 
		
# 		df_5 = pd.read_csv(fname_5, sep=',', index_col=False, usecols=[0,1,2])
# 		df_6 = pd.read_csv(fname_6, sep=',', index_col=False, usecols=[0,1,2])
# 		df_7 = pd.read_csv(fname_7, sep=',', index_col=False, usecols=[0,1,2])
# 		df_8 = pd.read_csv(fname_8, sep=',', index_col=False, usecols=[0,1,2])
# 		df_9 = pd.read_csv(fname_9, sep=',', index_col=False, usecols=[0,1,2])
# 		df_10 = pd.read_csv(fname_10, sep=',', index_col=False, usecols=[0,1,2])

# 		df_5['gname'] = g
# 		df_5['csize'] = 5
# 		df_6['gname'] = g
# 		df_6['csize'] = 6
# 		df_7['gname'] = g
# 		df_7['csize'] = 7
# 		df_8['gname'] = g
# 		df_8['csize'] = 8
# 		df_9['gname'] = g
# 		df_9['csize'] = 9
# 		df_10['gname'] = g
# 		df_10['csize'] = 10

# 		# df_5['run'] = range(20)
# 		# df_6['run'] = range(20)
# 		# df_7['run'] = range(20)
# 		# df_8['run'] = range(20)
# 		# df_9['run'] = range(20)
# 		# df_10['run'] = range(20)
		
		
# 		df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10], ignore_index=True)

# 		# print df 
# 		# df = df['gname','csize','estimate','succ_ratio']
# 		cols = df.columns.tolist()
# 		cols = cols[-1:] + cols[:-1]
		
# 		cols = cols[-1:] + cols[:-1]

# 		cols = cols[-1:] + cols[:-1]
# 		df = df[cols]

# 		# print df
# 		dfall = dfall.append(df)
# 		indexed_df_data = dfall.set_index(['gname','csize']) #,'run'])
# 		# print indexed_df_data

# 		indexed_df_data.to_csv('alldata50000.csv')

# 	return indexed_df_data



# def get_est_params():
# 	df_params = pd.DataFrame()

# 	for g in graphs:
# 		print 'yo'
# 		fname_5 = g +'_50000_5_0_params' 
# 		fname_6 = g +'_50000_6_0_params' 
# 		fname_7 = g +'_50000_7_0_params' 
# 		fname_8 = g +'_50000_8_0_params' 
# 		fname_9 = g +'_50000_9_0_params' 
# 		fname_10 = g +'_50000_10_0_params' 
		
# 		df_5 = pd.read_csv(fname_5, sep=',', index_col=False, usecols=range(0,7), nrows=1)
# 		df_6 = pd.read_csv(fname_6, sep=',', index_col=False, usecols=range(0,7), nrows=1)
# 		df_7 = pd.read_csv(fname_7, sep=',', index_col=False, usecols=range(0,7), nrows=1)
# 		df_8 = pd.read_csv(fname_8, sep=',', index_col=False, usecols=range(0,7), nrows=1)
# 		df_9 = pd.read_csv(fname_9, sep=',', index_col=False, usecols=range(0,7), nrows=1)
# 		df_10 = pd.read_csv(fname_10, sep=',', index_col=False, usecols=range(0,7), nrows=1)

# 		df_5['gname'] = g
# 		df_5['csize'] = 5
# 		df_6['gname'] = g
# 		df_6['csize'] = 6
# 		df_7['gname'] = g
# 		df_7['csize'] = 7
# 		df_8['gname'] = g
# 		df_8['csize'] = 8
# 		df_9['gname'] = g
# 		df_9['csize'] = 9
# 		df_10['gname'] = g
# 		df_10['csize'] = 10


# 		# x=[(g,i) for i in range(5,11,1)]

# 		# df = pd.DataFrame(x,columns=['gname','csize'])
# 		# print df
		
# 		df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10], ignore_index=True)

# 		# df = df['gname','csize','estimate','succ_ratio']
# 		cols = df.columns.tolist()
# 		print cols
# 		cols = cols[-1:] + cols[:-1]
		
# 		cols = cols[-1:] + cols[:-1]

# 		df = df[cols]

# 		# print df
# 		df_params = df_params.append(df)
# 		indexed_df_params = df_params.set_index(['gname','csize'])
# 		# print indexed_df_params

# 		indexed_df_params.to_csv('allparams50000.csv')
# 	return indexed_df_params


# 	# indexed_df = df.set_index(['gname','csize'])

# dfdata = get_est_data()
# # print 'dfdata'
# # print dfdata
# dfparams = get_est_params()
# df = pd.DataFrame()
# df = dfdata.ix[:,1:2].astype(float)
# print df
# df['time_tree'] = dfparams.ix[:,5:6]
# df['sample_time'] = dfdata.ix[:,0:1]
# df['time_sec'] = np.ceil((df['time_tree'] + df['sample_time']) / 1000000)
# df['time'] = pd.to_datetime(df['time_sec'], unit='s').dt.time
# # str(timedelta(microseconds=49503757))

# # df['time_minutes'] = np.floor(df['time_micro'] / 60000000)
# # df['time_seconds'] = np.ceil((df['time_micro'] - (60000000 * df['time_minutes'])) / 60)
# df['error'] = 0
# print df.columns
# df.columns = ['estimate', 'time_tree', 'sample_time', 'time_sec', 'time', 'error']
# df['estimate'] = df['estimate'].map('{:.2e}'.format)

# # print df.columns
# # print df

# df = df[[0,4,5]]
# stacked = df.stack()
# df = stacked.unstack(1)
# # df1 = df.ix[:,:].unstack(level=1)
# #df.unstack(level=0)
# # pd.set_option('float_format', '{:.2e}'.format)
# print df.columns

# # df['estimate'].map('${:.2e}'.format)
# df.to_csv('est_table.csv') #, float_format='%.2e')
# # print df.ix[:,3:4] #.map('${:.2f}'.format)

# print df
# # np.savetxt('est_table.csv',df,fmt='%1.3f',delimiter=',')
