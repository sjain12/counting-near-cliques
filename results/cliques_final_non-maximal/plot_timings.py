import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

pl.rcParams['text.usetex'] = True

graphs = ['loc-gowalla_edges', 'web-Stanford', 'amazon0601','com-youtube','web-Google','web-BerkStan','as-skitter','cit-Patents','soc-pokec-relationships','com-lj','com-orkut']

# for k in [7]:
df = pd.read_csv('est_table_all.csv', sep=',', index_col=[0,1]) #, usecols=[0,1])
y1 = []
y2 = []
for g in graphs:
	print df.ix[g].ix[7]['time']
	y1.append(df.ix[g].ix[7]['time'])
	print df.ix[g].ix[10]['time']
	y2.append(df.ix[g].ix[10]['time'])
# print df
# stacked = df.unstack(level=1)
# df = stacked 
# print df['time'][7]
# print df['time'][10]
# print df.index
#print df['time'][10]

fig, ax = pl.subplots()
# # pl.gcf().subplots_adjust(bottom=0.08)                                                               
pl.rcParams.update({'font.size': 22}) 
index = np.arange(len(graphs))+0.5
bar_width = 0.2
opacity = 0.8
ax.set_yscale("log")
names = ['loc-gow', 'web-Stan', 'amazon','youtube','Google','BerkStan','as-skitter','Patents','soc-pokec','com-lj','com-orkut']
# # df_timings.set_index(names, inplace='true')
rects1 = pl.bar(index, y1, bar_width,
                 alpha=opacity,
                 color='orange',
                 label='k=7', edgecolor = "orange")
 
rects2 = pl.bar(index + (1*bar_width), y2, bar_width,
                 alpha=opacity,
                 color='purple',
                 label='k=10', edgecolor = "purple")
# rects3 = pl.bar(index + (2*bar_width), df_timings['GRAFT'], bar_width,
#                  alpha=opacity,
#                  color='lightgreen',
#                  label='GRAFT', edgecolor = "lightgreen")

# 	# rects4 = pl.bar(index + (3*bar_width), df_timings['TS'], bar_width,
# 	#                  alpha=opacity,
# 	#                  color='m',
# 	#                  label='TS')
# 	# pl.xticks(index + bar_width, ('A', 'B', 'C', 'D'))
# 	# if k==7:
# 	# 	ax.set_ylim(ymax=20000)
# 	# else:
# 	# 	ax.set_ylim(ymax=30000)
	
pl.legend(loc=1, ncol=3, mode="expand", borderaxespad=0.,prop={'size':22}, frameon=False)
#           #ncol=2) ,bbox_to_anchor=(1.05, 1.2),
# ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
#       ncol=2)
ax.set_ylim(ymin=0.1, ymax = 1000000)
# 	# ax.legend(loc=9) #, bbox_to_anchor=(0.5, 1.1),
#           #ncol=2)
# 	# barlist[0].set_color('r')
pl.xticks(index+1, graphs)
pl.xticks(index + (bar_width), df.index, rotation='vertical') #graphs)
ax.set_xticklabels(names)

# 	pl.legend(loc=9,
#            ncol=3, mode="expand", borderaxespad=0.,prop={'size':22}, frameon=False)
# 	[xm, xM] = ax.get_xlim()
# 	# xM = ax.get_xmax()
# 	pl.plot([xm,xM], [1]*2, color='tomato')
# 	ax.set_xlim(xmin=0, xmax=11.5)
# 	# pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
ax.set_xlim(xmax=11.5)
ax.set_title('Timings')
ttl = ax.title
ttl.set_position([.5, 1.05])
ax.set_ylabel('Time in seconds')

ax.set_xlabel('Graphs')
pl.tight_layout()
pl.savefig('plots/timings.pdf', format='pdf')
pl.show()


