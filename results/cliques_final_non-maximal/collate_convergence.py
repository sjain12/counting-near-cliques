import numpy as np
import matplotlib.pyplot as pl
import matplotlib as mpl
import pandas as pd
import copy

pl.rcParams['text.usetex'] = True

# graphs = ['as-skitter','amazon0601','cit-Patents','web-Google','com-orkut','wiki-Talk','soc-LiveJournal1','soc-pokec-relationships','PaperReferences']
graphs = ['amazon0601','web-Google'] #,'orkut-links'] 'soc-LiveJournal1',,'com-lj',

# graphs = ['as-skitter','amazon0601']


# df_bf_data = pd.read_csv('bruteforcedata.csv', sep=',', index_col=[0,1])

def get_est_data_convergence():
	dfall = pd.DataFrame() #pd.read_csv('amazon0601_1000000_5_0_data')
	num_samples = [10000, 50000, 100000, 500000, 1000000]
	for g in graphs:
		# pring g
		for c in [7,10]: #range(5,11):
			# fig, ax = pl.subplots()
			fig = pl.figure()
			pl.gcf().subplots_adjust(bottom=0.15)                                                               
			ax = fig.add_subplot(1,1,1) 
			# pl.xlim([1,xmax])
			for n in num_samples:
				print 'yo'
				print g
				fname = g +'_'+str(n)+'_' + str(c) + '_100_0_data' 
				df = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])

				pl.scatter([n] * 100, df.ix[:, 0:1],color='blue')
				#ax.plot([n] * 100, df.ix[:, 0:1],color='blue')
			fname = g +'_'+str(c)+'_1_bf' 
			df_bf = pd.read_csv(fname, sep=',', index_col=False, usecols=[0,1,2])
			print df_bf
			actual = df_bf['estimate'][0]#df_bf_data.ix[g,0:1].iloc[c-5].item()
			print actual
			xmax = 2000000
			# pl.xlim(xmin=0, xmax=xmax)
			pl.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
			ax.plot([0,xmax], [actual] * 2, color='red')
			pl.rcParams.update({'font.size': 30})
			ax.set_xscale("log")
			xtics = copy.deepcopy(num_samples)
			xtics[:0] = [0]
 
			print xtics
			ax.set_xticks(xtics)
			pl.locator_params(axis='y',nbins=4)
			# ax.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter())
			
			ymax = 1.4*actual
			print ymax
			pl.xlim([5000,xmax])
			pl.ylim([0,ymax])
			ax.set_title(g+', k='+str(c))
			ax.set_ylabel('Cliques')
			ax.set_xlabel('Number of samples')
			ttl = ax.title
			ttl.set_position([.5, 1.2])
			# axes = pl.gca()
			# axes.set_xlim([0,xmax])
			# axes.set_ylim([0,ymax])
			pl.tight_layout()
			pl.savefig('plots/'+g+'_'+str(c)+'_convergence.pdf', format='pdf', transparent='True')
			pl.savefig('../../../../clique_sampling_paper/Figures/'+g+'_'+str(c)+'_convergence.pdf', format='pdf', transparent='True')
			pl.show()

get_est_data_convergence()

				# fname_6 = g +'_100000_6_100_0_data' 
				# fname_7 = g +'_100000_7_100_0_data' 
				# fname_8 = g +'_100000_8_100_0_data' 
				# fname_9 = g +'_100000_9_100_0_data' 
				# fname_10 = g +'_100000_10_100_0_data' 
			
			# df_5['gname'] = g
			# df_5['csize'] = 5
			# df_6['gname'] = g
			# df_6['csize'] = 6
			# df_7['gname'] = g
			# df_7['csize'] = 7
			# df_8['gname'] = g
			# df_8['csize'] = 8
			# df_9['gname'] = g
			# df_9['csize'] = 9
			# df_10['gname'] = g
			# df_10['csize'] = 10

			# df_5['run'] = range(100)
			# df_6['run'] = range(100)
			# df_7['run'] = range(100)
			# df_8['run'] = range(100)
			# df_9['run'] = range(100)
			# df_10['run'] = range(100)
			
			
		# 	df   = pd.concat([df_5, df_6, df_7, df_8, df_9, df_10], ignore_index=True)

		# 	# print df 
		# 	# df = df['gname','csize','estimate','succ_ratio']
		# 	cols = df.columns.tolist()
		# 	cols = cols[-1:] + cols[:-1]
			
		# 	cols = cols[-1:] + cols[:-1]

		# 	cols = cols[-1:] + cols[:-1]
		# 	df = df[cols]

		# 	# print df
		# 	dfall = dfall.append(df)
		# 	indexed_df_data = dfall.set_index(['gname','csize']) #,'run'])
		# 	# print indexed_df_data

		# 	indexed_df_data.to_csv('alldata100000.csv')

		# return indexed_df_data