#include <iostream>
#include <fstream>
#include <array>
#include <unistd.h>
#include <cstring>
#include "Escape/GraphIO.h"
#include "Escape/Digraph.h"
#include "Escape/CliquesBruteForce.h"
#include "Escape/nCr.h"

using namespace Escape;

void usage() {
    printf("\ntest_cliques_brute_force \n\t-i input_file_1[,input_file_2,...] \n\t-m min_clique_size \n\t-M max_clique_size \n");
}

int main(int argc, char *argv[])
{
    if(argc < 7) {
        usage();
        exit(1);
    }

    std::vector<std::string> graphs; // input files

    int min_clique_size = 0, max_clique_size = 0, runs = 0, i = 0;
    float start_threshold = 0.0, end_threshold = 0.0, threshold_interval = 0.0;
	int num_samples=0, full=0;
    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:m:M:")) != -1) {
        switch (c) {
            case 'i': {
                // multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {
                    graphs.push_back(std::string(token));
                    printf("%s,",token);
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'm':
                min_clique_size = atoi(optarg);
                cout << "Min Clique Size: " << min_clique_size << endl;
            break;

            case 'M':
                max_clique_size = atoi(optarg);
                cout << "Max Clique Size: " << max_clique_size << endl;
            break;

        }
    }
    printf("*****************************\n\n");

    populate_nCr();
    
    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;

        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
            exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);

        cout << "Degeneracy before doing degree ordering = " << degeneracy(cg) << endl;

        printf("Creating DAG\n"); // Note: Not degeneracy ordered!
        // CDAG dag = degreeOrdered(&cg);
        CDAG dag = degeneracyOrdered(&cg);



        cout << "\n vertices: " << g.nVertices << endl;
        cout << "edges = " << g.nEdges << std::endl;

        string f = *it;
        char *token = std::strtok(strdup(f.c_str()), ".");
        string gname(token);
        cout << "Graph name = " << gname << endl;

        vector<double> total_cliques(max_clique_size - min_clique_size + 1, 0);
        double num_cliques = 0;

        int max_deg = 0;
        for (i=0; i<cg.nVertices; i++)
            if (dag.outlist.degree(i) > max_deg) max_deg = dag.outlist.degree(i);
        cout << "Max deg = " << max_deg << endl;

        for(i=min_clique_size; i<=max_clique_size; i++)
        {
			string outfile(gname + "_" + to_string(i) + "_bf");
			std::string ofname = "../results/cliques/" + outfile;
    		std::cout << ofname << std::endl;
    		std::ofstream of;
    		of.open(ofname);
    		if (!of.is_open())
    		{
        		std::cout << "Could not open output file." << std::endl;
        		exit(1);
  			}
    		printf("Output File: %s\n", ofname.c_str());
			of << "csize,estimate,time,curr_time" << endl;
       	
		
        	num_cliques = cliques_brute_force(cg, dag, of, gname, "", i);
        	of << std::endl;
	
            total_cliques[i-min_clique_size] = num_cliques;
			of.close();
        }
        cout << "clique size, num cliques" << endl;
        for (i=min_clique_size; i<=max_clique_size; i++)
        {
            if (total_cliques[i-min_clique_size] != 0) cout << i << "," << total_cliques[i-min_clique_size] << endl;
        }
        cout << endl;
        delGraph(g);
        delCGraph(cg);
        delCDAG(dag);
    }
   


}
