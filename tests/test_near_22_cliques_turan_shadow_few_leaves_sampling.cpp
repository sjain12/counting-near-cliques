#include <iostream>
#include <fstream>
#include <array>
#include <unistd.h>
#include <cstring>
#include "Escape/GraphIO.h"
// #include "Escape/EdgeHash.h"
#include "Escape/Digraph.h"
#include "Escape/Near22CliquesTuranShadowFewLeavesSampling.h"
#include "Escape/nCr.h"

using namespace Escape;

void usage() {
    printf("\ntest_cliques_turan_shadow \n\t-i input_file_1[,input_file_2,...] \n\t-m min_near_clique_size \n\t-M max_near_clique_size \n\t-r runs \n\t-n num_samples\n");
}

int main(int argc, char *argv[])
{
    if(argc < 11) {
        usage();
        exit(1);
    }

    std::ofstream of; // output file
    std::vector<std::string> graphs; // input files
	srand (time(NULL));
    int min_nc_size = 0, max_nc_size = 0, runs = 0, i = 0;
    float start_threshold = 0.0, end_threshold = 0.0, threshold_interval = 0.0;
	int num_samples=0, full=0;
    char c;
    printf("*****************************\n");
    while ((c = getopt(argc, argv, "i:m:M:n:r:")) != -1) {
        switch (c) {
            case 'i': {
                // multiple input files
                char *token = std::strtok(optarg, ",");
                printf("Input Files: ");
                while (token != NULL) {
                    graphs.push_back(std::string(token));
                    printf("%s,",token);
                    token = std::strtok(NULL, ",");
                }
                printf("\n");
            }
            break;

            case 'm':
                min_nc_size = atoi(optarg);
                cout << "Min near-clique size: " << min_nc_size << endl;
            break;

            case 'M':
                max_nc_size = atoi(optarg);
                cout << "Max near-Clique Size:" << max_nc_size << endl;
            break;

            case 'n':
                num_samples = atoi(optarg);
                cout << "num_samples: " << num_samples << endl;
            break;

            case 'r':
                runs = atoi(optarg);
                cout << "Runs: " << runs << endl;
            break;

		}
    }
    printf("*****************************\n\n");
    
    populate_nCr();
    
    for (std::vector<std::string>::iterator it=graphs.begin(); it!=graphs.end(); ++it)
    {
        std::string fname = "../graphs/" + *it;
        std::cout << fname << std::endl;
        Graph g;
        printf("Loading graph\n");
        if (loadGraph(fname.c_str(), g, 1, IOFormat::escape))
            exit(1);

        printf("Converting to CSR\n");
        CGraph cg = makeCSR(g);

        printf("Creating DAG\n");
        CDAG dag = degeneracyOrdered(&cg);

        cout << "vertices: " << g.nVertices << endl;
        cout << "edges = " << g.nEdges << std::endl;

        string f = *it;
        char *token = std::strtok(strdup(f.c_str()), ".");
        string gname(token);
        cout << "Graph name = " << gname << endl;
        for(i=min_nc_size; i<=max_nc_size; i++)
        {
            near_22_cliques_turan_shadow_few_leaves_sampling(cg, dag, of, gname, "", runs, num_samples, i);
            of << " \n \n";
            cout << " \n \n";
        }
        of << std::endl;
        delGraph(g);
        delCGraph(cg);
        delCDAG(dag);
    }
    of.close();
}
