#include <iostream>
#include <random>
#include <vector>
#include <fstream>
#include <unistd.h>

using namespace std;

void usage() {
    printf("\ngenerate_graphs \n\t-n num_vertices \n\t-p prob_edge \n");
}

int main(int argc, char *argv[])
{
    if(argc < 5) {
        usage();
        exit(1);
    }

    int n;
    double p;
    char c;

    printf("*****************************\n");
    while ((c = getopt(argc, argv, "n:p:")) != -1) {
        switch (c) {
            // case 'o': {
            //     // multiple input files
            //     char *token = std::strtok(optarg, ",");
            //     printf("Output Files: ");
            //     while (token != NULL) {
            //         graphs.push_back(std::string(token));
            //         printf("%s,",token);
            //         token = std::strtok(NULL, ",");
            //     }
            //     printf("\n");
            // }
            // break;

            case 'n':
                n = atoi(optarg);
                cout << "Number of vertices: " << n << endl;
            break;

            case 'p':
                p = atof(optarg);
                cout << "p: " << p << endl;
            break;

            // case 'r':
            //     runs = atoi(optarg);
            //     cout << "Runs: " << runs << endl;
            // break;

        }
    }
    printf("*****************************\n\n");

    string outfile("er_" + to_string(n) + "_" + to_string(p) +".edges");
	//string outfile(gname + "_maximal_bk");
	std::string ofname = "../graphs/" + outfile;
	std::cout << ofname << std::endl;
	std::ofstream of;
	of.open(ofname);
	if (!of.is_open())
	{
		std::cout << "Could not open output file." << std::endl;
		exit(1);
	}
	printf("Output File: %s\n", ofname.c_str());

	srand (time(NULL));

	vector<int> is, js;
	is.reserve(n*n);
	js.reserve(n*n);
	std::random_device rd;
    std::mt19937 gen(rd());
    // give "true" 1/4 of the time
    // give "false" 3/4 of the time
    std::bernoulli_distribution d(p);

	for (int i=0; i<n-1; i++)
	{
		for (int j=i+1; j<n; j++)
		{
			if (d(gen)) 
			{
				is.push_back(i);
				js.push_back(j);
			}
		}
	}

	of << n << " " << is.size() << endl;
	for (int i=0; i<is.size(); i++)
	{
		of << is[i] << " " << js[i] << endl;
	}

	of.close();
}
